﻿using System;

namespace HTToolkit.BLE.NordicDFU
{
    public class DFUException : Exception
    {
        const string COMMAND_NOT_SUCCEED_TEMPLATE = "Failed to execute DFU command. "
                                                    + "Command code: {0}";

        public DFUException() { }
        public DFUException(CommandCode command)
                : base(GetExceptionString(command))
        {
        }

        public DFUException(CommandCode command, ObjectType type)
                : base(GetExceptionString(command, type))
        {
        }

        protected static string GetExceptionString(CommandCode command)
        {
            var template = COMMAND_NOT_SUCCEED_TEMPLATE;
            return string.Format(template, command.ToString());
        }

        protected static string GetExceptionString(CommandCode command, ObjectType type)
        {
            var template = COMMAND_NOT_SUCCEED_TEMPLATE
                           + ", Object type: {1}";
            return string.Format(template, command.ToString(), type.ToString());
        }
    }

    public class DFUNotSucceedException : DFUException
    {
        public DFUNotSucceedException(CommandCode command) 
                : base(command)
        {
        }

        public DFUNotSucceedException(CommandCode command, ObjectType type) 
                : base(command, type)
        {
        }
    }

    public class DFUTimeoutException : DFUException
    {
        public DFUTimeoutException(CommandCode command)
               : base(command)
        {
        }

        public DFUTimeoutException(CommandCode command, ObjectType type)
                : base(command, type)
        {
        }
    }

    public class DFUTooManyRetriesException : DFUException
    {
        public DFUTooManyRetriesException(CommandCode command)
               : base(command)
        {
        }

        public DFUTooManyRetriesException(CommandCode command, ObjectType type)
                : base(command, type)
        {
        }
    }
}
