﻿using System;

namespace HTToolkit.BLE.Peripheral
{
    public class PeripheralEventDispatcher
    {
        public event EventHandler<(string, byte[])> WriteRequestsReceived;
        public event EventHandler Paired;

        public void RaiseWriteRequestsReceived(string uuid, byte[] value)
        {
            WriteRequestsReceived?.Invoke(this, (uuid, value));
        }
        public void RaisePaired()
        {
            Paired?.Invoke(this, null);
        }
    }
}
