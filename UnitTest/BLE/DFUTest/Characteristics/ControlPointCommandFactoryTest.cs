﻿using System;
using HTToolkit.BLE.NordicDFU.Characteristics;
using Xunit;
using System.Linq;

namespace DFUTest
{
    public class ControlPointCommandFactoryTest
    {
        [Fact]
        public void SelectCommandObjectTest()
        {
            byte[] expected = { 06, 01 };
            byte[] actual = ControlPointCommandFactory.GetBytesOfSelectCommandObject();
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void SelectDataObjectTest()
        {
            byte[] expected = { 06, 02 };
            byte[] actual = ControlPointCommandFactory.GetBytesOfSelectDataObject();
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void CreateCommandObjectTest()
        {
            var size = 100u;
            byte[] expected = { 01, 01, 100, 0, 0, 0 };
            byte[] actual = ControlPointCommandFactory.GetBytesOfCreateCommandOject(size);
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void CreateDataObjectTest()
        {
            var size = 100u;
            byte[] expected = { 01, 02, 100, 0, 0, 0 };
            byte[] actual = ControlPointCommandFactory.GetBytesOfCreateDataOject(size);
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void CalculateCRCTest()
        {
            byte[] expected = { 03 };
            byte[] actual = ControlPointCommandFactory.GetBytesOfCalculateCRC();
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void ExecuteTest()
        {
            byte[] expected = { 04 };
            byte[] actual = ControlPointCommandFactory.GetBytesOfExecute();
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void SetPRNValueTest()
        {
            ushort value = 12;
            byte[] expected = { 02, 12, 0};
            byte[] actual = ControlPointCommandFactory.GetBytesOfSetPRNValue(value);
            Assert.True(actual.SequenceEqual(expected));
        }
    }
}
