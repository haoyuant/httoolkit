﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HTToolkit.BLE.Central
{
    public abstract class CentralManagerBase : IBLEEventDelegate
    {
        protected ICentralManager centralManager;
        protected Dictionary<string, HTCharacteristic> characteristics;

        TaskCompletionSource<Task> waitUntilReadyToUseTask;

        protected CentralManagerBase(ICentralManager centralManager)
        {
            this.centralManager = centralManager;
            this.centralManager.BLEEventDelegate = this;

            characteristics = new Dictionary<string, HTCharacteristic>();
        }

        #region Common Activities to Bluetooth Device
        public virtual bool TryScan(string serviceUUID)
        {
            return centralManager.TryScan(serviceUUID);
        }

        public virtual void StopScan()
        {
            centralManager.StopScan();
        }

        public virtual Task<PeripheralProfile> Connect(string identifier, int timeout = 0)
        {
            return centralManager.Connect(identifier, timeout);
        }

        public virtual Task Disconnect(int timeout = 0)
        {
            return centralManager.Disconnect(timeout);
        }

        public virtual Task WaitUntilReadyToUseCharacteristic()
        {
            if (waitUntilReadyToUseTask != null)
                return Task.CompletedTask;

            waitUntilReadyToUseTask = new TaskCompletionSource<Task>();
            return waitUntilReadyToUseTask.Task;
        }
        #endregion

        #region Common Callbacks from Bluetooth Device
        public virtual void OnDiscoveredDevice(PeripheralProfile deviceInfo)
        {

        }

        public virtual void OnDiscoveredCharacteristic()
        {
            if (waitUntilReadyToUseTask == null)
                waitUntilReadyToUseTask = new TaskCompletionSource<Task>();
            waitUntilReadyToUseTask?.TrySetResult(Task.CompletedTask);
            foreach (var characteristic in characteristics)
            {
                characteristic.Value.OnReadyToUse();
            }
        }

        public virtual void OnWrote(string uuid, bool success)
        {
            if (characteristics.ContainsKey(uuid))
            {
                var characteristic = characteristics[uuid];
                characteristic.OnWrote(success);
            }
        }

        public virtual void OnValueUpdated(CharacteristicUpdate update)
        {
            var uuid = update.UUID;
            var data = update.Data;
            if (characteristics.ContainsKey(uuid))
            {
                var characteristic = characteristics[uuid];
                characteristic.OnValueUpdated(data);
            }
        }
        #endregion
    }
}
