﻿using HTToolkit.BLE.Peripheral;

namespace HTToolkit.BLE.iOS.Peripheral
{
    public class PeripheralManager : IPeripheralManager
    {
        readonly PeripheralManagerDelegate bpm;

        public PeripheralManager()
        {
            bpm = new PeripheralManagerDelegate();
        }

        public void SetDispatcher(PeripheralEventDispatcher dispatcher)
        {
            bpm.Dispatcher = dispatcher;
        }

        public byte[] GetValue(string uuid)
        {
            return bpm.GetValue(uuid);
        }


        public void StartAdvertising(ServiceTree tree)
        {
            bpm.StartAdvertising(tree);
        }

        public void StopAdvertising()
        {
            bpm.StopAdvertising();
        }

        public bool UpdateValue(byte[] data, string uuid)
        {
            return bpm.UpdateValue(data, uuid);
        }

        public void Release()
        {
            bpm.Release();
        }
    }
}
