using System;
using Xunit;
using HTToolkit.PacketBuffer;
using System.Linq;

namespace PacketBuffer.Test
{
    public class PacketBufferTest
    {
        [Fact]
        public void AcceptOneValidPacketTest()
        {
            bool isInvoked = false;
            byte[] parsedPacket = null;

            byte[] packet1 = { 58, 1, 2, 3, 5, 1, 2, 3, 4, 5, 9, 9, 77 };
            var buffer = new HeaderPacketBuffer
            {
                HeadDelimiter = 58,
                TailDelimiter = 77,
                PrefixBytes = 3,
                LengthBytes = 5,
                SuffixBytes = 2
            };
            buffer.PacketReceived += (sender, e) =>
            {
                isInvoked = true;
                parsedPacket = e;
            };
            buffer.Accept(packet1);

            Assert.True(isInvoked);
            Assert.True(packet1.SequenceEqual(parsedPacket));
        }

        [Fact]
        public void AcceptValidSegmentsTest()
        {
            var invokedCount = 0;
            byte[] parsedPacket = null;
            byte[] segment1 = { 58, 1, 2, 3};
            byte[] segment2 = { 5, 1, 2, 3, 4, 5 };
            byte[] segment3 = { 9, 9, 77 };
            var buffer = new HeaderPacketBuffer
            {
                HeadDelimiter = 58,
                TailDelimiter = 77,
                PrefixBytes = 3,
                LengthBytes = 5,
                SuffixBytes = 2
            };
            buffer.PacketReceived += (sender, e) =>
            {
                ++invokedCount;
                parsedPacket = e;
            };
            buffer.Accept(segment1);
            buffer.Accept(segment2);
            buffer.Accept(segment3);
            byte[] expected = { 58, 1, 2, 3, 5, 1, 2, 3, 4, 5, 9, 9, 77 };

            Assert.Equal(1, invokedCount);
            Assert.True(expected.SequenceEqual(parsedPacket));
        }
    }
}
