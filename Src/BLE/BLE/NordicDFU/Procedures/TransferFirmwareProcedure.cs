﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HTToolkit.BLE.NordicDFU.Characteristics;

namespace HTToolkit.BLE.NordicDFU.Procedure
{
    public class TransferFirmwareProcedure : DFUProcedure
    {
        byte[] firmware;
        List<byte[]> firmwareParts = new List<byte[]>();
        int currentPartIdx;
        bool firmwareSent;

        int verifiedBytesSent;
        int unverifiedBytesSent;
        double _currentProgress;
        double CurrentProgress
        {
            get => _currentProgress;
            set
            {
                _currentProgress = value;
                RaiseProgressUpdated(_currentProgress);
            }
        }

        void UpdateProgress()
        {
            CurrentProgress = (double)(verifiedBytesSent + unverifiedBytesSent) / firmware.Length;
        }

        public event EventHandler<double> ProgressUpdated;
        public void RaiseProgressUpdated(double progress)
        {
            ProgressUpdated?.Invoke(this, progress);
        }

        public TransferFirmwareProcedure(ControlPointCharacteristic controlPoint,
            PacketCharacteristic packetCharac,
            byte[] firmware)
            : base(controlPoint, packetCharac)
        {
            this.firmware = firmware;
        }

        public override async Task<bool> Start()
        {
            // Read current data object info.
            var dataObject = await controlPoint.SelectDataObject();

            InitSendingProgress(dataObject.MaxSize, dataObject.Offset);

            await CheckSendingProgress(dataObject.MaxSize, dataObject.Offset, dataObject.CRC32);

            return await completeTask.Task;
        }

        void InitSendingProgress(uint maxPartLen, uint offset)
        {
            // Split the firmware image into binary parts
            firmwareParts = DFUUtility.SplitFirmwareParts(firmware, maxPartLen);
            // Reset the packet sending status
            currentPartIdx = (int)DFUUtility.CalculatePartIdx(offset, maxPartLen);
            verifiedBytesSent = (int)offset;
        }

        async Task CheckSendingProgress(uint maxPartLen, uint offset, uint crc)
        {
            var remainder = offset % maxPartLen;
            // Have not sent any data object
            if (offset == 0)
            {
                await SendNewPart(currentPartIdx);
            }
            // The CRC of sent data is verified
            else await VerifySentData(maxPartLen, offset, crc, remainder);
        }

        async Task VerifySentData(uint maxPartLen, uint offset, uint crc, uint remainder)
        {
            if (DFUUtility.VerifyCRC(firmware, offset, crc))
            {
                await ContinueSendingProgress(maxPartLen, offset, remainder);
            }
            // The CRC of sent data is not verified, need to resend current packet.
            else
            {
                await ResendCorruptedPart(maxPartLen, remainder);
            }
        }

        async Task ContinueSendingProgress(uint maxPartLen, uint offset, uint remainder)
        {
            UpdateProgress();
            // The whole firmware is sent
            if (offset == firmware.Length)
            {
                firmwareSent = true;
                await Execute();
            }
            // Part of the firmware is sent
            else
            {
                // A whole packet is sent
                if (remainder == 0)
                {
                    //currentPartIdx -= 1;
                    await Execute();
                }
                // Part of a packet is sent
                else
                {
                    await ResumeSendingPart(offset, maxPartLen);
                }
            }
        }

        async Task ResendCorruptedPart(uint maxPartLen, uint remainder)
        {
            if (remainder == 0)
                verifiedBytesSent -= (int)maxPartLen;
            else
                verifiedBytesSent -= (int)remainder;

            UpdateProgress();

            await SendNewPart(currentPartIdx);
        }

        async Task SendNewPart(int partIdx)
        {
            await controlPoint.SetPRNValue(40);
            var part = firmwareParts[partIdx];
            await controlPoint.CreateDataObject((uint)part.Length);
            await SendPart(part);
        }

        async Task ResumeSendingPart(uint offset, uint partLength)
        {
            var part = DFUUtility.GetDataNeedToBeSentOfPart(firmwareParts, offset, partLength);
            await SendPart(part);
        }

        async Task SendPart(byte[] part)
        {
            await SendPartInSmallUnits(part);
            await VerifySentPart();
        }

        async Task SendPartInSmallUnits(byte[] part)
        {
            int skip = 0;
            var packetSize = packetCharac.PacketSize;
            while (true)
            {
                var dataToSendNow = GetSmallUnit(part, skip, packetSize);
                if (dataToSendNow == null)
                    break;

                SendSmallUnit(dataToSendNow);

                skip = await VerifySentSmallUnit(part, skip);
                if (skip == -1)
                    break;
            }
        }

        async Task<int> VerifySentSmallUnit(byte[] part, int skip)
        {
            var response = await controlPoint.CalculateCRC();
            var rOffset = response.Offset;
            var rCRC = response.CRC32;
            var offsetToVerify = rOffset - verifiedBytesSent;

            if (DFUUtility.VerifyCRC(firmware, rOffset, rCRC))
            {
                var remain = part.Length - offsetToVerify;
                if (remain != 0)
                {
                    UpdateProgress();
                    skip = (int)offsetToVerify;
                    await controlPoint.ExecuteCommand();
                    return skip;
                }
                return -1;
            }
            return -1;
        }

        void SendSmallUnit(byte[] dataToSendNow)
        {
            packetCharac.SendPacket(dataToSendNow);
            unverifiedBytesSent += dataToSendNow.Length;

            Debug.WriteLine("Packet Characteristic: {0} bytes has been sent.", dataToSendNow.Length);
        }

        byte[] GetSmallUnit(byte[] part, int skip, uint packetSize)
        {
            var dataToSent = part.Skip(skip).ToArray();
            var objectSizeInPackets = (dataToSent.Length + packetSize - 1) / packetSize;

            var prnValue = controlPoint.PRNValue;
            var packetToSendNow = Math.Min(prnValue, objectSizeInPackets);

            if (prnValue == 0)
                packetToSendNow = objectSizeInPackets;
            if (packetToSendNow == 0)
                return null;

            var dataToSendNow = dataToSent.Take((int)(packetToSendNow * packetSize)).ToArray();
            return dataToSendNow;
        }

        async Task VerifySentPart()
        {
            var respone = await controlPoint.CalculateCRC();
            var offset = respone.Offset;
            var crc = respone.CRC32;

            if (crc != 0 && DFUUtility.VerifyCRC(firmware, offset, crc))
            {
                firmwareSent = offset == firmware.Length;
                verifiedBytesSent += unverifiedBytesSent;
                unverifiedBytesSent = 0;
                UpdateProgress();
                await Execute();
            }
            else
            {
                await SendNewPart(currentPartIdx);
            }
        }

        async Task Execute()
        {
            await controlPoint.ExecuteCommand();
            if (firmwareSent)
            {
                //Done, now the device should reset itself.
                Console.WriteLine("Firmware update is done.");
                completeTask?.TrySetResult(true);
            }
            else
            {
                currentPartIdx += 1;
                await SendNewPart(currentPartIdx);
            }
        }
    }
}
