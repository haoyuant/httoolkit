﻿using System;
using System.Collections.Generic;
using CoreBluetooth;
using Foundation;
using HTToolkit.BLE.Peripheral;

namespace HTToolkit.BLE.iOS.Peripheral
{
    public class PeripheralManagerDelegate : CBPeripheralManagerDelegate
    {
        CBPeripheralManager pm;
        readonly Dictionary<CBUUID, CBMutableCharacteristic> characteristicDict;

        CBCentral ConnectedCentral;

        ServiceTree tree;

        bool isAwaitingPowerOn;

        public PeripheralEventDispatcher Dispatcher { get; set; }

        public PeripheralManagerDelegate()
        {
            pm = new CBPeripheralManager(this, null);
            characteristicDict = new Dictionary<CBUUID, CBMutableCharacteristic>();
        }

        #region Exposed methods

        public void StartAdvertising(ServiceTree tree)
        {
            this.tree = tree;
            if (pm.State == CBPeripheralManagerState.PoweredOn)
            {
                InitServicesTree();
                pm.StartAdvertising(GetStartAdvertisingOptions(tree.Advertising));
            }
            else
            {
                isAwaitingPowerOn = true;
            }
        }

        public void StopAdvertising()
        {
            pm.StopAdvertising();
        }

        public bool UpdateValue(byte[] data, string uuid)
        {
            var cbUuid = CBUUID.FromString(uuid);
            if (GetMutableCharacteristic(cbUuid) is CBMutableCharacteristic characteristic)
            {
                var nsData = NSData.FromArray(data);
                return pm.UpdateValue(nsData, characteristic, null);
            }

            return false;
        }

        public byte[] GetValue(string uuid)
        {
            var cbUuid = CBUUID.FromString(uuid);
            return GetMutableCharacteristic(cbUuid) is CBMutableCharacteristic characteristic ?
                characteristic.Value?.ToArray() :
                null;
        }

        public void Release()
        {
            // Dispose NSObject to release resources
            ConnectedCentral.Dispose();
            ConnectedCentral = null;
            // Release this peripheral manager
            pm.Dispose();
            pm = null;
        }

        #endregion

        void InitServicesTree()
        {
            foreach (var service in tree.Services)
            {
                var sid = CBUUID.FromString(service.UUID);
                var cbService = new CBMutableService(sid, true);
                var characteristics = new CBMutableCharacteristic[service.Characteristics.Length];
                int i = 0;
                foreach (var characteristic in service.Characteristics)
                {
                    var cid = CBUUID.FromString(characteristic.UUID);
                    var properties = GetCBCharacteristicProperties(characteristic);
                    var permissions = GetCBAttributePermissions(characteristic);
                    var cbCharacteristic = new CBMutableCharacteristic(cid, properties, null, permissions);

                    characteristics[i++] = cbCharacteristic;
                    characteristicDict.Add(cbCharacteristic.UUID, cbCharacteristic);
                }
                cbService.Characteristics = characteristics;

                pm.AddService(cbService);
            }
        }

        #region Delegate methods
        public override void StateUpdated(CBPeripheralManager peripheral)
        {
            switch (peripheral.State)
            {
                case CBPeripheralManagerState.PoweredOn:
                    Console.WriteLine("Peripheral is on.");
                    if (isAwaitingPowerOn)
                    {
                        StartAdvertising(tree);

                        isAwaitingPowerOn = false;
                    }
                    break;
            }
        }

        public override void ServiceAdded(CBPeripheralManager peripheral, CBService service, NSError error)
        {
            if (error != null)
            {
                Console.WriteLine(error.DebugDescription);
            }
        }

        public override void AdvertisingStarted(CBPeripheralManager peripheral, NSError error)
        {
            if (error != null)
            {
                Console.WriteLine(error.DebugDescription);
            }
            else
            {
                Console.WriteLine("Started Advertisting.");
            }
        }

        public override void ReadRequestReceived(CBPeripheralManager peripheral, CBATTRequest request)
        {
            if (request.Central.Identifier == ConnectedCentral.Identifier)
            {
                if (GetMutableCharacteristic(request.Characteristic.UUID) is CBMutableCharacteristic characteristic)
                {
                    if (characteristic.Value == null)
                    {
                        pm.RespondToRequest(request, CBATTError.InvalidAttributeValueLength);
                        return;
                    }

                    if ((nuint)request.Offset > characteristic.Value.Length)
                    {
                        pm.RespondToRequest(request, CBATTError.InvalidOffset);
                    }
                    else
                    {
                        var dataRange = new NSRange(request.Offset, (nint)characteristic.Value.Length - request.Offset);
                        request.Value = characteristic.Value.Subdata(dataRange);

                        pm.RespondToRequest(request, CBATTError.Success);
                    }
                }
            }
            else
            {
                Console.WriteLine("Rejected central: {0}'s read request", request.Central.Identifier);
            }
        }

        public override void WriteRequestsReceived(CBPeripheralManager peripheral, CBATTRequest[] requests)
        {
            foreach (var request in requests)
            {
                if (request.Central.Identifier == ConnectedCentral.Identifier)
                {
                    if (GetMutableCharacteristic(request.Characteristic.UUID) is CBMutableCharacteristic characteristic)
                    {
                        //if (characteristic.Value != null && (nuint)request.Offset > characteristic.Value.Length)
                        //{
                        //    pm.RespondToRequest(request, CBATTError.InvalidOffset);
                        //    return;
                        //}
                        //characteristic.Value = request.Value;\
                    }
                    pm.RespondToRequest(request, CBATTError.Success);

                    Dispatcher.RaiseWriteRequestsReceived(request.Characteristic.UUID.ToString(),
                                                          request.Value.ToArray());
                }
                else
                {
                    Console.WriteLine("Rejected central: {0}'s write request", request.Central.Identifier);
                }
            }
        }

        public override void CharacteristicSubscribed(CBPeripheralManager peripheral, CBCentral central, CBCharacteristic characteristic)
        {
            pm.StopAdvertising();

            Console.WriteLine("Central subscribed to characteristic {0}", characteristic);
            pm.SetDesiredConnectionLatency(CBPeripheralManagerConnectionLatency.Low, central);

            if (ConnectedCentral == null)
            {
                ConnectedCentral = central;
                Console.WriteLine("Connected central: {0}", central.Identifier);

                Dispatcher.RaisePaired();
            }
        }

        public override void CharacteristicUnsubscribed(CBPeripheralManager peripheral, CBCentral central, CBCharacteristic characteristic)
        {
            Console.WriteLine("unpaired.");
        }
        #endregion

        CBAttributePermissions GetCBAttributePermissions(Characteristic characteristic)
        {
            CBAttributePermissions permissions = 0;
            foreach (var permission in characteristic.Permissions)
            {
                switch (permission)
                {
                    case "readable":
                        permissions |= CBAttributePermissions.Readable;
                        break;
                    case "writeable":
                        permissions |= CBAttributePermissions.Writeable;
                        break;
                }
            }

            return permissions;
        }

        CBCharacteristicProperties GetCBCharacteristicProperties(Characteristic characteristic)
        {
            CBCharacteristicProperties properties = 0;
            foreach (var property in characteristic.Properties)
            {
                switch (property)
                {
                    case "read":
                        properties |= CBCharacteristicProperties.Read;
                        break;
                    case "write":
                        properties |= CBCharacteristicProperties.WriteWithoutResponse;
                        break;
                    case "notify":
                        properties |= CBCharacteristicProperties.Notify;
                        break;
                    case "indicate":
                        properties |= CBCharacteristicProperties.Indicate;
                        break;
                }
            }

            return properties;
        }

        StartAdvertisingOptions GetStartAdvertisingOptions(string[] advertising)
        {
            int i = 0;
            var cbUuids = new CBUUID[advertising.Length];
            foreach (var uuid in advertising)
            {
                cbUuids[i++] = CBUUID.FromString(uuid);
            }

            return new StartAdvertisingOptions
            {
                LocalName = CBAdvertisement.DataServiceUUIDsKey,
                ServicesUUID = cbUuids
            };
        }

        CBMutableCharacteristic GetMutableCharacteristic(CBUUID uuid)
        {
            return characteristicDict.ContainsKey(uuid) ?
                                    characteristicDict[uuid] :
                                    null;
        }
    }
}
