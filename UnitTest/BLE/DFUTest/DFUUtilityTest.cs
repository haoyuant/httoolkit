using System;
using Xunit;
using HTToolkit.BLE.NordicDFU;
using HTToolkit.BLE;
using System.Collections.Generic;
using System.Linq;

namespace DFUTest
{
    public class DFUUtilityTest
    {
        [Fact]
        public void SplitFirmwarePartsTest()
        {
            var firmware = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var packetLength = 3u;
            var parts = DFUUtility.SplitFirmwareParts(firmware, packetLength);
            var target = new List<byte[]>
            {
                new byte[]{0,1,2},
                new byte[]{3,4,5},
                new byte[]{6,7,8},
                new byte[]{9}
            };
            Assert.Equal(parts.Count, target.Count);
            for(var i = 0; i < parts.Count; ++i)
            {
                for(var j = 0; j < parts[i].Length; ++j)
                {
                    Assert.Equal<int>(parts[i][j], target[i][j]);
                }
            }
        }

        [Fact]
        public void GetRemainDataOfPartTest()
        {
            var firmware = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var packetLength = 3u;
            var parts = DFUUtility.SplitFirmwareParts(firmware, packetLength);
            var offset = 4u;
            var remainData = DFUUtility.GetDataNeedToBeSentOfPart(parts, offset, packetLength);
            var target = new byte[] { 4, 5 };
            Assert.Equal(remainData.Length, target.Length);
            for(var i = 0; i < remainData.Length; ++i)
            {
                Assert.Equal<int>(remainData[i], target[i]);
            }
        }

        [Fact]
        public void CompareArrayTest()
        {
            byte[] arr1 = { 1, 2, 3 };
            byte[] arr2 = { 1, 2, 3 };
            byte[] arr3 = { 1, 2 };
            byte[] arr4 = { 1, 2, 3, 4 };
            byte[] arr5 = { };
            Assert.True(CompareArray(arr1, arr2));
            Assert.False(CompareArray(arr1, arr3));
            Assert.False(CompareArray(arr1, arr4));
            Assert.False(CompareArray(arr1, arr5));
            Assert.False(CompareArray(arr1, null));
        }

        [Fact]
        public void CRCTest()
        {
            byte[] data = { 10,9,8,7,6,5,4,3,2,1,0 };
            var crc = Crc32.CRC32Bytes(data);
            Assert.Equal(2905444065, crc);
        }

        public bool CompareArray<T>(IEnumerable<T> arr1, IEnumerable<T> arr2)
        {
            if (arr1 == null || arr2 == null)
                return false;

            if (arr1.Count() != arr2.Count())
                return false;

            for (var i = 0; i < arr1.Count(); ++i)
            {
                if (!arr1.ElementAt(i).Equals(arr2.ElementAt(i)))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
