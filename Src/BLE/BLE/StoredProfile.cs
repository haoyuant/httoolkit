﻿using System;

namespace HTToolkit.BLE
{
    public class StoredProfile
    {
        public string Uuid { get; set; }
        public string Name { get; set; }

        public StoredProfile(string uuid, string name)
        {
            Uuid = uuid;
            Name = name;
        }
    }
}
