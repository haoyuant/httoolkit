﻿using System;
namespace HTToolkit.BLE.NordicDFU
{
    public interface IDFUDelegate
    {
        void OnNewDFUTargetFound(PeripheralProfile info);
        void OnProgressUpdated(double progress);
        void OnPacketResent();
        void OnInitPacketSent();
        void OnFirmwareUpdated();
    }
}
