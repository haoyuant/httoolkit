﻿using System;
using System.Linq;
using Xunit;
using Moq;
using HTToolkit.BLE.NordicDFU.Characteristics;
using HTToolkit.BLE.Central;
using System.Threading.Tasks;
using HTToolkit.BLE.NordicDFU;
using System.Collections.Generic;

namespace DFUTest
{
    public class ControlPointCharacteristicTest
    {
        [Fact]
        public async Task SelectCommandObjectTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var controlPoint = new ControlPointCharacteristic(cmMock.Object, "abc");

            var expected = new SelectResponse
            {
                MaxSize = 100,
                Offset = 20,
                CRC32 = 123456789
            };
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    var buffer = new List<byte>
                    {
                        0x60, 06, 01
                    };
                    buffer.AddRange(BitConverter.GetBytes(expected.MaxSize));
                    buffer.AddRange(BitConverter.GetBytes(expected.Offset));
                    buffer.AddRange(BitConverter.GetBytes(expected.CRC32));
                    controlPoint.OnWrote(true);
                    controlPoint.OnValueUpdated(buffer.ToArray());
                });
            var actual = await controlPoint.SelectCommandObject();

            Assert.Equal(actual.MaxSize, expected.MaxSize);
            Assert.Equal(actual.Offset, expected.Offset);
            Assert.Equal(actual.CRC32, expected.CRC32);
        }

        [Fact]
        public async Task SelectDataObjectTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var controlPoint = new ControlPointCharacteristic(cmMock.Object, "abc");

            var expected = new SelectResponse
            {
                MaxSize = 100,
                Offset = 20,
                CRC32 = 123456789
            };
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    var buffer = new List<byte>
                    {
                        0x60, 06, 02
                    };
                    buffer.AddRange(BitConverter.GetBytes(expected.MaxSize));
                    buffer.AddRange(BitConverter.GetBytes(expected.Offset));
                    buffer.AddRange(BitConverter.GetBytes(expected.CRC32));
                    controlPoint.OnWrote(true);
                    controlPoint.OnValueUpdated(buffer.ToArray());
                });
            var actual = await controlPoint.SelectDataObject();
            
            Assert.Equal(actual.MaxSize, expected.MaxSize);
            Assert.Equal(actual.Offset, expected.Offset);
            Assert.Equal(actual.CRC32, expected.CRC32);
        }

        [Fact]
        public async Task CreateCommandObjectTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var controlPoint = new ControlPointCharacteristic(cmMock.Object, "abc");

            var expected = true;
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    var response = new byte[] { 0x60, 01, 01 };
                    controlPoint.OnWrote(true);
                    controlPoint.OnValueUpdated(response);
                });
            var actual = await controlPoint.CreateCommandObject(100);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task CreateDataObjectTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var controlPoint = new ControlPointCharacteristic(cmMock.Object, "abc");

            var expected = true;
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    var response = new byte[] { 0x60, 01, 01 };
                    controlPoint.OnWrote(true);
                    controlPoint.OnValueUpdated(response);
                });
            var actual = await controlPoint.CreateDataObject(100);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task CalculateCRCTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var controlPoint = new ControlPointCharacteristic(cmMock.Object, "abc");

            var expected = new CRCResponse
            {
                Offset = 20,
                CRC32 = 123456789
            };
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    var buffer = new List<byte>
                    {
                        0x60, 03, 01
                    };
                    buffer.AddRange(BitConverter.GetBytes(expected.Offset));
                    buffer.AddRange(BitConverter.GetBytes(expected.CRC32));
                    controlPoint.OnWrote(true);
                    controlPoint.OnValueUpdated(buffer.ToArray());
                });
            var actual = await controlPoint.CalculateCRC();

            Assert.Equal(expected.Offset, actual.Offset);
            Assert.Equal(expected.CRC32, actual.CRC32);
        }

        [Fact]
        public async Task ExecuteTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var controlPoint = new ControlPointCharacteristic(cmMock.Object, "abc");

            var expected = true;
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    var response = new byte[] { 0x60, 04, 01 };
                    controlPoint.OnWrote(true);
                    controlPoint.OnValueUpdated(response);
                });
            var actual = await controlPoint.ExecuteCommand();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task SetPRNValueTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var controlPoint = new ControlPointCharacteristic(cmMock.Object, "abc");

            ushort value = 12;
            var expected = ControlPointCommandFactory.GetBytesOfSetPRNValue(value);
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    Assert.True(data.SequenceEqual(expected));
                    controlPoint.OnWrote(true);
                });

            await controlPoint.SetPRNValue(value);
        }

        [Fact]
        public void LittleEndianTest()
        {
            Assert.True(BitConverter.IsLittleEndian);
        }
    }
}
