﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HTToolkit.PacketBuffer
{
    public interface IPacketBuffer
    {
        void Accept(byte[] data);

        event EventHandler<byte[]> PacketReceived;
    }
}
