﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using HTToolkit.BLE.NordicDFU.Characteristics;

namespace HTToolkit.BLE.NordicDFU.Procedure
{
    public class TransferInitPacketProcedure : DFUProcedure
    {
        byte[] initPacket;

        public TransferInitPacketProcedure(ControlPointCharacteristic controlPoint,
            PacketCharacteristic packetCharac,
            byte[] initPacket)
            : base(controlPoint, packetCharac)
        {
            this.initPacket = initPacket;
        }

        //TransferState transferState;

        public override async Task<bool> Start()
        {
            // Read command object info
            var commandObject = await controlPoint.SelectCommandObject();

            await CheckSendingProgress(commandObject.Offset, commandObject.CRC32);

            return await completeTask.Task;
        }

        async Task CheckSendingProgress(uint offset, uint crc)
        {
            if (offset == 0)
            {// Nothing has been sent, should create a new command object and start a new update
                await StartNewUpdate(initPacket);
            }
            else if (DFUUtility.VerifyCRC(initPacket, offset, crc))
            {// At least part of the packet has been sent and CRC is valid
                if (offset < initPacket.Length && offset > 0)
                {// Resume sending remaining data of the init packet
                    await SendInitPacket(offset);
                }
                else if (offset == initPacket.Length)
                {// The whole packet has been sent, should execute it 
                    await Execute();
                }
            }
            else
            {// CRC is not valid, should resend the whole packet
                await StartNewUpdate(initPacket);
            }
        }

        async Task StartNewUpdate(byte[] data)
        {
            await controlPoint.SetPRNValue(0);
            await controlPoint.CreateCommandObject((uint)data.Length);
            await SendInitPacket(0);
        }

        async Task SendInitPacket(uint offset)
        {
            packetCharac.SendInitPacket(initPacket, offset);
            await VerifySentPacket();
        }

        async Task VerifySentPacket()
        {
            var crcResult = await controlPoint.CalculateCRC();
            var resultOffset = crcResult.Offset;
            var resultCRC = crcResult.CRC32;
            if (DFUUtility.VerifyCRC(initPacket, (uint)initPacket.Length, resultCRC))
            {
                await Execute();
            }
            else
            {
                await StartNewUpdate(initPacket);
            }
        }

        async Task Execute()
        {
            await controlPoint.ExecuteCommand();
            completeTask?.TrySetResult(true);
        }
    }
}
