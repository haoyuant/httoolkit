﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MVVM.Test.Views;
using HTToolkit.MVVM;
using Autofac;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MVVM.Test
{
    public partial class App : Application
    {
        public App()
        {
            ServiceLocator.Builder.RegisterType<HierarchyNavigationService>().As<INavigationService>();
            ServiceLocator.Container = ServiceLocator.Builder.Build();

            InitializeComponent();
            MainPage = new NavigationPage(new MainView());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
