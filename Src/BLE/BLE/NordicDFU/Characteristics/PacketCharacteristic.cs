﻿using System;
using HTToolkit.BLE;
using HTToolkit.BLE.Central;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HTToolkit.BLE.NordicDFU.Characteristics
{
    public class PacketCharacteristic : HTCharacteristic
    {
        public uint PacketSize { get; private set; }

        public PacketCharacteristic(ICentralManager centralManager, string uuid, uint packetSize = 20) 
                : base(centralManager, uuid)
        {
            PacketSize = packetSize;
        }

        public void SendInitPacket(byte[] data, uint offset)
        {
            var bytesToSend = data.Skip((int)offset).ToArray();
            SendPacket(bytesToSend);
        }

        public void SendPacket(byte[] data)
        {
            var offset = 0;
            var bytesToSend = data.Length;
            if (bytesToSend == 0)
            {
                return;
            }
            do
            {
                var packetLength = (int)Math.Min(PacketSize, bytesToSend);
                var packet = data.Skip(offset).Take(packetLength).ToArray();
                Write(packet, false);

                offset += packetLength;
                bytesToSend -= packetLength;

            } while (bytesToSend > 0);
        }
    }
}
