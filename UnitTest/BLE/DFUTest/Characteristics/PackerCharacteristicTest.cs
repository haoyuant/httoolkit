﻿using System;
using System.Linq;
using Xunit;
using HTToolkit.BLE.NordicDFU.Characteristics;
using Moq;
using HTToolkit.BLE.Central;
using HTToolkit.BLE.NordicDFU;
using System.Collections.Generic;

namespace DFUTest
{
    public class PackerCharacteristicTest
    {
        [Fact]
        public void SendPacketTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var packetCharac = new PacketCharacteristic(cmMock.Object, "abc");

            var packet = new byte[100];
            for(var i = 0; i < 100; ++i)
            {
                packet[i] = (byte)i;
            }
            var parts = new List<byte[]>
            {
                packet.Take(20).ToArray(),
                packet.Skip(20).Take(20).ToArray(),
                packet.Skip(40).Take(20).ToArray(),
                packet.Skip(60).Take(20).ToArray(),
                packet.Skip(80).Take(20).ToArray()
            };
            var partIdx = 0;
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    Assert.True(data.SequenceEqual(parts[partIdx++]));
                });
            packetCharac.SendPacket(packet);
        }

        [Fact]
        public void SendInitPacketTest()
        {
            var cmMock = new Mock<ICentralManager>();
            var packetCharac = new PacketCharacteristic(cmMock.Object, "abc");

            var packet = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            var expected = new byte[] { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            cmMock.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<bool>()))
                .Callback((string uuid, byte[] data, bool withResponse) =>
                {
                    Assert.True(data.SequenceEqual(expected));
                });
            packetCharac.SendInitPacket(packet, 5);
        }
    }
}
