﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace HTToolkit.BLE
{
    public class ProfileDatabase
    {
        static string Key = "StoredProfile6";

        static ProfileDatabase database;

        static object lockThis = new object();

        Dictionary<string, string> storedProfiles;

        ProfileDatabase() 
        {
            if (Application.Current.Properties.ContainsKey(Key))
            {
                var serializedContents = Application.Current.Properties[Key] as string;
                storedProfiles = Deserialize(serializedContents);
            }
            else
            {
                storedProfiles = new Dictionary<string, string>();
                Application.Current.Properties.Add(Key,storedProfiles);
                Application.Current.SavePropertiesAsync();
            }
        }

        public static ProfileDatabase Shared()
        {
            lock(lockThis)
            {
                if (database == null)
                {
                    database = new ProfileDatabase();
                }
                return database;
            }
        }

        public Dictionary<string, string> Retrieve()
        {
            return storedProfiles;
        }

        public void Add((string uuid, string name) profile)
        {
            if (!storedProfiles.ContainsKey(profile.uuid))
            {
                storedProfiles.Add(profile.uuid, profile.name);
                var json = Serialize(storedProfiles);
                Application.Current.Properties[Key] = json;
                Application.Current.SavePropertiesAsync();

                var app = Application.Current;
            }
        }

        public void Remove(string uuid)
        {
            if (storedProfiles.ContainsKey(uuid))
            {
                storedProfiles.Remove(uuid);
                Application.Current.Properties[Key] = storedProfiles;
                Application.Current.SavePropertiesAsync();
            }
        }

        string Serialize(Dictionary<string, string> profiles)
        {
            var entries = "";
            foreach (var profile in profiles)
            {
                entries += string.Format("{0}:{1}\n", profile.Key, profile.Value);
            }
            return entries;
        }

        Dictionary<string, string> Deserialize(string serializedContents)
        {
            var entries = serializedContents.Split('\n');
            var dict = new Dictionary<string, string>();
            foreach (var entry in entries)
            {
                if (!string.IsNullOrEmpty(entry))
                {
                    var pair = entry.Split(':');
                    dict.Add(pair[0], pair[1]);
                }
            }
            return dict;
        }


    }
}
