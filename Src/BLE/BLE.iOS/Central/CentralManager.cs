﻿using System;
using CoreBluetooth;
using Foundation;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;
using HTToolkit.BLE.Central;

namespace HTToolkit.BLE.iOS.Central
{
    public class CentralManager : ICentralManager
    {
        readonly CBCentralManager centralManager;

        internal CBPeripheral ConnectedPeripheral { get; set; }

        internal Dictionary<string, CBPeripheral> storedPeripherals = new Dictionary<string, CBPeripheral>();
        internal Dictionary<string, CBPeripheral> newlyDiscoveredPeripherals = new Dictionary<string, CBPeripheral>();

        internal Dictionary<string, CBCharacteristic> discoveredCharacteristics = new Dictionary<string, CBCharacteristic>();

        internal Dictionary<string, TaskCompletionSource<byte[]>> readTasks = new Dictionary<string, TaskCompletionSource<byte[]>>();
        internal TaskCompletionSource<PeripheralProfile> connectTask = new TaskCompletionSource<PeripheralProfile>();
        internal TaskCompletionSource<Task> disconnectTask = new TaskCompletionSource<Task>();

        public IBLEEventDelegate BLEEventDelegate { get; set; }

        public CentralManager()
        {
            var periheralDelegate = new BDNCPeripheral(this);
            var centralDelegate = new CentralManagerDelegate(this, periheralDelegate);
            centralManager = new CBCentralManager(centralDelegate, null);
        }

        #region IPeripheralDiscoveryService, IPeripheralInteractionService Implementation
        /// <summary>
        /// Try starting BLE peripheral scanning
        /// </summary>
        /// <returns><c>true</c>, if scanning, <c>false</c> otherwise.</returns>
        /// <param name="serviceUUID">Scan for peripherals with given service uuid.</param>
        public bool TryScan(string serviceUUID)
        {
            if (centralManager.State == CBCentralManagerState.PoweredOn)
            {
                if (serviceUUID != null)
                {
                    centralManager.ScanForPeripherals(CBUUID.FromString(serviceUUID));
                }
                else
                {
                    centralManager.ScanForPeripherals(peripheralUuids: null);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Stops the scan.
        /// </summary>
        public void StopScan()
        {
            centralManager.StopScan();
        }

        /// <summary>
        /// Retrieves the devices.
        /// </summary>
        /// <param name="uuids">Uuids.</param>
        public void RetrieveDevices(List<string> uuids)
        {
            var nsuuids = new NSUuid[uuids.Count];
            for (int i = 0; i < uuids.Count; ++i)
            {
                nsuuids[i] = new NSUuid(uuids[i]);
            }
            centralManager.RetrievePeripheralsWithIdentifiers(nsuuids);
        }

        /// <summary>
        /// Connect to a peripheral with the given uuid
        /// </summary>
        /// <returns>The connect.</returns>
        /// <param name="uuid">UUID of the peripheral</param>
        /// <param name="timeout">Timeout.</param>
        public Task<PeripheralProfile> Connect(string uuid, int timeout = 0)
        {
            connectTask = new TaskCompletionSource<PeripheralProfile>();
            SetTimeoutToConnectTask(connectTask, timeout);

            var targetPeripheral = FindInAvailablePeripherals(uuid);
            if (targetPeripheral != null)
            {
                centralManager.ConnectPeripheral(targetPeripheral);
            }

            return connectTask.Task;
        }

        /// <summary>
        /// Disconnect with the current peripheral
        /// </summary>
        /// <returns>The disconnect.</returns>
        Task ICentralManager.Disconnect(int timeout)
        {
            disconnectTask = new TaskCompletionSource<Task>();
            SetTimeoutToDisconnectTask(disconnectTask, timeout);
            centralManager.CancelPeripheralConnection(ConnectedPeripheral);

            return disconnectTask.Task;
        }

        /// <summary>
        /// Disable all characteristic notification
        /// </summary>
        public void Clean()
        {
            if (ConnectedPeripheral != null)
            {
                foreach (var service in ConnectedPeripheral.Services)
                {
                    foreach (var characteristic in service.Characteristics)
                    {
                        if (characteristic.IsNotifying)
                        {
                            ConnectedPeripheral.SetNotifyValue(false, characteristic);
                        }
                    }
                }
            }
            storedPeripherals.Clear();
            newlyDiscoveredPeripherals.Clear();
        }

        /// <summary>
        /// Read the specified characteristicUUID and timeout.
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="characteristicUUID">Characteristic UUID.</param>
        /// <param name="timeout">Timeout.</param>
        public Task<byte[]> Read(string characteristicUUID, int timeout = 0)
        {
            var readTask = GetReadTask(characteristicUUID);
            SetTimeoutToReadTask(readTask, timeout);
            ReadValueOfCharacteristic(characteristicUUID);
            return readTask.Task;
        }

        /// <summary>
        /// Subscribe or unsubscribe the specified characteristic with the given uuid.
        /// </summary>
        /// <param name="characteristicUUID">Characteristic UUID.</param>
        /// <param name="enabled">If set to <c>true</c> enabled.</param>
        public void Subscribe(string characteristicUUID, bool enabled)
        {
            var characteristic = discoveredCharacteristics[characteristicUUID];
            ConnectedPeripheral.SetNotifyValue(enabled, characteristic);
        }

        public void Write(string characteristicUUID, byte[] data, bool withResponse)
        {
            var value = NSData.FromArray(data);
            WriteValueToCharacteristic(characteristicUUID, value, withResponse);
        }

        public void Write(string characteristicUUID, string json, bool withResponse)
        {
            var value = NSData.FromString(json);
            WriteValueToCharacteristic(characteristicUUID, value, withResponse);
        }
        #endregion

        #region Interal supported methods
        void ReadValueOfCharacteristic(string uuid)
        {
            var characteristic = GetCharacteristic(uuid);
            ConnectedPeripheral.ReadValue(characteristic);
        }

        TaskCompletionSource<byte[]> GetReadTask(string characteristicUUID)
        {
            if (readTasks.ContainsKey(characteristicUUID))
            {
                return readTasks[characteristicUUID];
            }

            var readTask = new TaskCompletionSource<byte[]>();
            readTasks.Add(characteristicUUID, readTask);

            return readTask;
        }

        void SetTimeoutToReadTask(TaskCompletionSource<byte[]> readTask, int timeout)
        {
            ExecuteAtTimeout(timeout, () =>
            {
                readTask.TrySetResult(null);
            });
        }

        CBPeripheral FindInAvailablePeripherals(string uuid)
        {
            storedPeripherals.TryGetValue(uuid, out var targetPeripheral);
            if (targetPeripheral == null)
            {
                newlyDiscoveredPeripherals.TryGetValue(uuid, out targetPeripheral);
            }

            return targetPeripheral;
        }

        void SetTimeoutToConnectTask(TaskCompletionSource<PeripheralProfile> task, int timeout)
        {
            ExecuteAtTimeout(timeout, () =>
            {
                task.TrySetResult(null);
            });

        }

        void SetTimeoutToDisconnectTask(TaskCompletionSource<Task> task, int timeout)
        {
            ExecuteAtTimeout(timeout, () =>
            {
                task.TrySetResult(Task.CompletedTask);
            });

        }

        CBCharacteristic GetCharacteristic(string uuid)
        {
            if (ConnectedPeripheral == null)
            {
                return null;
            }
            if (discoveredCharacteristics.ContainsKey(uuid))
            {
                return discoveredCharacteristics[uuid];
            }
            return null;
        }

        void WriteValueToCharacteristic(string uuid, NSData value, bool withResponse)
        {
            discoveredCharacteristics.TryGetValue(uuid, out var characteristic);
            var writeType = withResponse ? CBCharacteristicWriteType.WithResponse :
                                           CBCharacteristicWriteType.WithoutResponse;
            ConnectedPeripheral.WriteValue(value, characteristic, writeType);
        }


        void ExecuteAtTimeout(int timeout, Action action)
        {
            if (timeout > 0)
            {
                Task.Run(async () =>
                {
                    await Task.Delay(timeout);
                    action();
                });
            }
        }
        #endregion
    }


    #region CentralManagerDelegate
    public class CentralManagerDelegate : CBCentralManagerDelegate
    {
        CentralManager manager;
        BDNCPeripheral peripheralDelegate;

        public CentralManagerDelegate(CentralManager manager, BDNCPeripheral peripheralDelegate)
        {
            this.manager = manager;
            this.peripheralDelegate = peripheralDelegate;
        }

        override public void UpdatedState(CBCentralManager central)
        {
            switch (central.State)
            {
                case CBCentralManagerState.PoweredOn:
                    break;
                default:
                    central.StopScan();
                    break;
            }
        }

        override public void DiscoveredPeripheral(CBCentralManager central, CBPeripheral peripheral, Foundation.NSDictionary advertisementData, Foundation.NSNumber RSSI)
        {
            if (!manager.storedPeripherals.ContainsKey(peripheral.Identifier.AsString())
                && !manager.newlyDiscoveredPeripherals.ContainsKey(peripheral.Identifier.AsString()))
            {
                manager.newlyDiscoveredPeripherals.Add(peripheral.Identifier.AsString(), peripheral);

                var pinfo = new PeripheralProfile
                {
                    Identifier = peripheral.Identifier.ToString(),
                    Name = peripheral.Name,
                    RSSI = RSSI.Int32Value
                };
                manager.BLEEventDelegate.OnDiscoveredDevice(pinfo);
            }
        }

        public override void RetrievedPeripherals(CBCentralManager central, CBPeripheral[] peripherals)
        {
            foreach (var peripheral in peripherals)
            {
                manager.storedPeripherals.Add(peripheral.Identifier.AsString(), peripheral);
            }
        }

        override public void ConnectedPeripheral(CBCentralManager central, CBPeripheral peripheral)
        {
            central.StopScan();
            manager.ConnectedPeripheral = peripheral;
            var profile = new PeripheralProfile
            {
                Identifier = peripheral.Identifier.AsString(),
                Name = peripheral.Name
            };
            manager.connectTask?.TrySetResult(profile);

            peripheral.Delegate = peripheralDelegate;
            peripheral.DiscoverServices();
        }

        public override void DisconnectedPeripheral(CBCentralManager central, CBPeripheral peripheral, NSError error)
        {
            manager.disconnectTask?.TrySetResult(Task.CompletedTask);
            manager.ConnectedPeripheral = null;
        }
    }
    #endregion


    #region PeripheralDelegate
    public class BDNCPeripheral : CBPeripheralDelegate
    {
        CentralManager manager;

        public BDNCPeripheral(CentralManager manager)
        {
            this.manager = manager;
        }

        override public void DiscoveredService(CBPeripheral peripheral, NSError error)
        {
            foreach (var service in peripheral.Services)
            {
                Console.WriteLine(service);
                peripheral.DiscoverCharacteristics(service);
            }
        }

        public override void DiscoveredCharacteristic(CBPeripheral peripheral, CBService service, NSError error)
        {
            foreach (var characteristic in service.Characteristics)
            {
                if (!manager.discoveredCharacteristics.ContainsKey(characteristic.UUID.Uuid))
                {
                    manager.discoveredCharacteristics.Add(characteristic.UUID.Uuid, characteristic);
                }
            }
            manager.BLEEventDelegate.OnDiscoveredCharacteristic();
        }

        public override void UpdatedCharacterteristicValue(CBPeripheral peripheral, CBCharacteristic characteristic, NSError error)
        {
            var update = new CharacteristicUpdate
            {
                UUID = characteristic.UUID.Uuid,
                Data = ConvertNsDataToBytes(characteristic.Value)
            };
            if (manager.readTasks.ContainsKey(characteristic.UUID.Uuid))
            {
                manager.readTasks[characteristic.UUID.Uuid].TrySetResult(update.Data);
            }
            manager.BLEEventDelegate.OnValueUpdated(update);
        }

        public override void WroteCharacteristicValue(CBPeripheral peripheral, CBCharacteristic characteristic, NSError error)
        {
            var success = false;
            if (error == null)
            {
                success = true;
            }
            manager.BLEEventDelegate.OnWrote(characteristic.UUID.Uuid, success);
        }

        byte[] ConvertNsDataToBytes(NSData value)
        {
            var rawData = value.Bytes;
            byte[] bytes = new byte[value.Length];
            Marshal.Copy(rawData, bytes, 0, (int)value.Length);

            return bytes;
        }
    }
    #endregion
}
