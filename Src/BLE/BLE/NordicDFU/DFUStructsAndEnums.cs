﻿using System;
namespace HTToolkit.BLE.NordicDFU
{
    public enum CommandCode : byte
    {
        Create = 0x01,
        SetReceiptNotification = 0x02,
        CRC = 0x03,
        Execute = 0x04,
        Select = 0x06,
        Response = 0x60,
    }

    public enum CommandResultCode
    {
        Fail = 0x00,
        Success = 0x01
    }

    public enum ObjectType : byte
    {
        Command = 0x01,
        Data = 0x02
    }

    public struct CRCResponse
    {
        public UInt32 Offset;
        public UInt32 CRC32;
    }
    public struct SelectResponse
    {
        public UInt32 MaxSize;
        public UInt32 Offset;
        public UInt32 CRC32;
    }

}
