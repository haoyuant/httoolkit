﻿using System;
using Xunit;
using HTToolkit.BLE.NordicDFU.Firmware;
using System.IO;
using System.Linq;

namespace DFUTest.Firmware
{
    public class DFUApplicationTest
    {
        [Fact]
        public void FromZipTest()
        {
            var path = @"Firmware/flite_receiver_app_V15_dfu_package_v 1.21-0-g563558f.zip";

            var fileName = @"flite_receiver_app_V15_dfu_package_v 1.21-0-g563558f.zip";
            var type = DFUFirmwareType.Application;
            var size = "102.66 kB";
            var version = Version.Parse("1.2.1");

            var info = new DFUFirmwareInfo(fileName, type, size, version);

            var initPacket = File.ReadAllBytes("Firmware/nrf52832_xxaa.dat");
            var firmware = File.ReadAllBytes("Firmware/nrf52832_xxaa.bin");

            if (File.Exists(path))
            {
                var expected = new DFUApplication(info, initPacket, firmware);
                var actual = DFUApplication.FromZip(path);
                Assert.NotNull(actual);
                Assert.Equal(actual.Info.FileName, expected.Info.FileName);
                Assert.Equal(actual.Info.Type, expected.Info.Type);
                Assert.Equal(actual.Info.Size, expected.Info.Size);
                Assert.Equal(actual.Info.Version, expected.Info.Version);
                Assert.True(actual.InitPacket.SequenceEqual(expected.InitPacket));
                Assert.True(actual.Firmware.SequenceEqual(expected.Firmware));
            }
        }
    }
}
