﻿using System;
using System.Threading.Tasks;
using HTToolkit.BLE.Central;

namespace HTToolkit.BLE
{
    public class HTCharacteristic
    {
        readonly protected ICentralManager centralManager;

        public string UUID { get; private set; }

        TaskCompletionSource<bool> writeResponseTask;

        public HTCharacteristic(ICentralManager centralManager, string uuid)
        {
            this.centralManager = centralManager;
            UUID = uuid;
        }

        public void Read(int timeout = 0)
        {
            centralManager.Read(UUID, timeout);
        }

        public void Write(byte[] data, bool withRespone)
        {
            centralManager.Write(UUID, data, withRespone);
        }

        public Task<bool> WriteWithResponseAsync(byte[] data)
        {
            writeResponseTask = new TaskCompletionSource<bool>();
            Write(data, true);
            return writeResponseTask.Task;
        }

        public void Subscribe(bool enabled)
        {
            centralManager.Subscribe(UUID, enabled);
        }

        public virtual void OnReadyToUse()
        {

        }

        public virtual void OnWrote(bool success)
        {
            writeResponseTask?.TrySetResult(success);
        }

        public virtual void OnValueUpdated(byte[] data)
        {

        }
    }
}
