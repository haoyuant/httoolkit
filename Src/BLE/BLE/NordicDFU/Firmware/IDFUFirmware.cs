﻿using System;
namespace HTToolkit.BLE.NordicDFU.Firmware
{
    public interface IDFUFirmware
    {
        DFUFirmwareInfo Info { get; }
        byte[] InitPacket { get; }
        byte[] Firmware { get; }
    }

    public struct DFUFirmwareInfo
    {
        public string FileName { get; }
        public DFUFirmwareType Type { get; }
        public string Size { get; }
        public Version Version { get; }

        public DFUFirmwareInfo(string name, DFUFirmwareType type, string size, Version version)
        {
            FileName = name;
            Type = type;
            Size = size;
            Version = version;
        }
    }
}
