﻿using System;
using HTToolkit.BLE;
using HTToolkit.BLE.NordicDFU;
using Moq;
using System.Linq;
using System.Collections.Generic;
using Xunit;

namespace DFUTest
{
    public interface IDFUTarget
    {
        SelectResponse SelectCommandObject();
        SelectResponse SelectDataObject();
        void CreateCommandObject(uint size);
        void CreateDataObject(uint size);
        CRCResponse CalculateCRC();
        void Execute();
        void OnReceivedData(byte[] data);
        void SetPRNValue(UInt16 value);
    }
}
