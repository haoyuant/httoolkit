﻿using System;
namespace HTToolkit.BLE
{
    public class PeripheralProfile
    {
        public string Identifier { get; set; }
        public string Name { get; set; }
        public int RSSI { get; set; }
    }
}
