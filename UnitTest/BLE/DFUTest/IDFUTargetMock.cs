﻿using System;
using System.Collections.Generic;
using HTToolkit.BLE.NordicDFU;
using HTToolkit.BLE;
using Moq;

namespace DFUTest
{

    public class IDFUTargetMock
    {
        public SelectResponse CommandObject;
        public SelectResponse DataObject;

        public List<byte> InitPacket;
        public List<byte> CurrentFirmwarePart;
        public List<List<byte>> FirmwareParts;

        public UInt16 PRNValue;

        DFUCentral dfuCentral;

        public bool IsInitPacketSent;

        CRCResponse crcResponse;

        public IDFUTargetMock(DFUCentral central)
        {
            dfuCentral = central;
            FirmwareParts = new List<List<byte>>();
        }

        public IDFUTarget Mock()
        {
            CommandObject = new SelectResponse();
            DataObject = new SelectResponse();

            var mock = new Mock<IDFUTarget>();
            // Setup SELECT_COMMAND_OBJECT
            mock.Setup(m => m.SelectCommandObject())
                .Returns(() =>
                {
                    CommandObject.Offset = InitPacket == null ? 0 : (uint)InitPacket.Count;
                    CommandObject.CRC32 = InitPacket == null ? 0 : Crc32.CRC32Bytes(InitPacket.ToArray());
                    return CommandObject;
                })
                .Callback(() =>
                {
                    byte[] data = ToBytes(CommandObject);
                    var update = new CharacteristicUpdate
                    {
                        UUID = DFUCentral.SECURE_DFU_CONTROL_POINT,
                        Data = data
                    };
                    dfuCentral?.OnValueUpdated(update);
                });
            // Setup SELECT_DATA_OBJECT
            mock.Setup(m => m.SelectDataObject())
                .Returns(() =>
                {
                    var dataReceived = new List<byte>();
                    if (FirmwareParts.Count > 0)
                    {
                        foreach (var part in FirmwareParts)
                        {
                            dataReceived.AddRange(part);
                        }
                    }
                    if (CurrentFirmwarePart != null)
                    {
                        dataReceived.AddRange(CurrentFirmwarePart);
                    }
                    DataObject.MaxSize = 30;
                    DataObject.Offset = (uint)dataReceived.Count;
                    DataObject.CRC32 = dataReceived.Count == 0 ? 0 : Crc32.CRC32Bytes(dataReceived.ToArray());
                    return DataObject;
                })
                .Callback(() =>
                {
                    byte[] data = ToBytes(DataObject);
                    var update = new CharacteristicUpdate
                    {
                        UUID = DFUCentral.SECURE_DFU_CONTROL_POINT,
                        Data = data
                    };
                    dfuCentral?.OnValueUpdated(update);
                });
            // Setup CREATE_COMMAND_OBJECT
            mock.Setup(m => m.CreateCommandObject(It.IsAny<uint>()))
                .Callback<uint>((size) =>
                {
                    CommandObject = new SelectResponse();
                    if (InitPacket == null)
                        InitPacket = new List<byte>();
                    byte[] data = { 0x60, 01, 01 };
                    var update = new CharacteristicUpdate
                    {
                        UUID = DFUCentral.SECURE_DFU_CONTROL_POINT,
                        Data = data
                    };
                    dfuCentral?.OnValueUpdated(update);
                });
            // Setup CREATE_DATA_OBJECT
            mock.Setup(m => m.CreateDataObject(It.IsAny<uint>()))
                .Callback<uint>((size) =>
                {
                    DataObject = new SelectResponse();
                    //if (CurrentFirmwarePart == null)
                    CurrentFirmwarePart = new List<byte>();
                    byte[] data = { 0x60, 01, 01 };
                    var update = new CharacteristicUpdate
                    {
                        UUID = DFUCentral.SECURE_DFU_CONTROL_POINT,
                        Data = data
                    };
                    dfuCentral?.OnValueUpdated(update);
                });
            // Setup ON_RECEIVED_DATA
            mock.Setup(m => m.OnReceivedData(It.IsAny<byte[]>()))
                .Callback<byte[]>((data) =>
                {
                    if (!IsInitPacketSent)
                    {
                        InitPacket.AddRange(data);
                    }
                    else
                    {
                        CurrentFirmwarePart.AddRange(data);
                    }
                });
            // Setup CALCULATE_CRC
            mock.Setup(m => m.CalculateCRC())
                .Returns(() =>
                {
                    if (!IsInitPacketSent)
                    {
                        var response = new CRCResponse
                        {
                            Offset = InitPacket == null ? 0 : (uint)InitPacket.Count,
                            CRC32 = InitPacket == null ? 0 : Crc32.CRC32Bytes(InitPacket.ToArray()),
                        };
                        crcResponse = response;
                        return response;
                    }
                    else
                    {
                        var dataReceived = new List<byte>();
                        if (FirmwareParts.Count > 0)
                        {
                            foreach (var part in FirmwareParts)
                            {
                                dataReceived.AddRange(part);
                            }
                        }
                        if (CurrentFirmwarePart != null)
                        {
                            dataReceived.AddRange(CurrentFirmwarePart);
                        }
                        var response = new CRCResponse
                        {
                            Offset = (uint)dataReceived.Count,
                            CRC32 = dataReceived.Count == 0 ? 0 : Crc32.CRC32Bytes(dataReceived.ToArray()),
                        };
                        crcResponse = response;
                        return response;
                    }
                })
                .Callback(() =>
                {
                    var data = ToBytes(crcResponse);
                    var update = new CharacteristicUpdate
                    {
                        UUID = DFUCentral.SECURE_DFU_CONTROL_POINT,
                        Data = data
                    };
                    dfuCentral?.OnValueUpdated(update);
                });
            // Setup EXECUTE
            mock.Setup(m => m.Execute())
                .Callback(() =>
                {
                    if (!IsInitPacketSent)
                    {
                        IsInitPacketSent = true;
                    }
                    else
                    {
                        if (CurrentFirmwarePart != null)
                        {
                            FirmwareParts.Add(CurrentFirmwarePart);
                            CurrentFirmwarePart = null;
                        }
                    }
                    byte[] data = { 0x60, 04, 01 };
                    var update = new CharacteristicUpdate
                    {
                        UUID = DFUCentral.SECURE_DFU_CONTROL_POINT,
                        Data = data
                    };
                    dfuCentral?.OnValueUpdated(update);
                });
            // Setup SET_PRN_VALUE
            mock.Setup(m => m.SetPRNValue(It.IsAny<UInt16>()))
                .Callback<UInt16>((value) =>
                {
                    PRNValue = value;
                });
            return mock.Object;
        }

        public byte[] ToBytes(SelectResponse response)
        {
            var data = new List<byte>
            {
                0x60, 06, 01,
            };
            data.AddRange(BitConverter.GetBytes(response.MaxSize));
            data.AddRange(BitConverter.GetBytes(response.Offset));
            data.AddRange(BitConverter.GetBytes(response.CRC32));
            return data.ToArray();
        }

        public byte[] ToBytes(CRCResponse response)
        {
            var data = new List<byte>
            {
                0x60, 03, 01,
            };
            data.AddRange(BitConverter.GetBytes(response.Offset));
            data.AddRange(BitConverter.GetBytes(response.CRC32));
            return data.ToArray();
        }
    }
}
