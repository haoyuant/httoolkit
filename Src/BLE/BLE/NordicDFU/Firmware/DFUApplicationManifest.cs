﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace HTToolkit.BLE.NordicDFU.Firmware
{
    [JsonObject]
    public class DFUApplicationManifest
    {
        [JsonProperty("manifest")]
        public DFUManifestJSON Manifest { get; set; }
    }

    [JsonObject]
    public class DFUManifestJSON
    {
        [JsonProperty("application")]
        public DFUApplicationJSON Application { get; set; }

        [JsonProperty("version"), JsonConverter(typeof(VersionConverter))]
        public Version Version { get; set; }
    }

    [JsonObject]
    public class DFUApplicationJSON
    {
        [JsonProperty("bin_file")]
        public string Bin { get; set; }
        [JsonProperty("dat_file")]
        public string Dat { get; set; }
    }
}
