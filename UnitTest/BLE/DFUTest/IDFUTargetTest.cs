﻿using System;
using System.Collections.Generic;
using System.Linq;
using HTToolkit.BLE.NordicDFU;
using HTToolkit.BLE;
using Xunit;

namespace DFUTest
{

    public class IDFUTargetTest
    {

        [Fact]
        public void SelectCommandObjectTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            byte[] data = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            mock.InitPacket = new List<byte>();
            mock.InitPacket.AddRange(data);

            var response = target.SelectCommandObject();
            Assert.Equal(0u, response.MaxSize);
            Assert.Equal(10u, response.Offset);
            Assert.Equal(Crc32.CRC32Bytes(data), response.CRC32);
        }

        [Fact]
        public void SelectDataObjectTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            byte[] data = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            mock.CurrentFirmwarePart = new List<byte>();
            mock.CurrentFirmwarePart.AddRange(data);
            //mock.IsInitPacketSent = true;
            //target.Execute();

            var response = target.SelectDataObject();
            Assert.Equal(30u, response.MaxSize);
            Assert.Equal(16u, response.Offset);
            Assert.Equal(Crc32.CRC32Bytes(data), response.CRC32);
        }

        [Fact]
        public void CreateCommandObjectTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            target.CreateCommandObject(100u);

            Assert.NotNull(mock.InitPacket);
        }

        [Fact]
        public void CreateDataObjectTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            target.CreateDataObject(100u);

            Assert.NotNull(mock.CurrentFirmwarePart);
        }

        [Fact]
        public void CalculateCRCTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            byte[] initPacket = { 0, 1, 2, 3, 4, 5, 6, 7 };
            byte[] firmwarePart = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            mock.InitPacket = new List<byte>();
            mock.InitPacket.AddRange(initPacket);
            mock.CurrentFirmwarePart = new List<byte>();
            mock.CurrentFirmwarePart.AddRange(firmwarePart);

            mock.IsInitPacketSent = false;
            var initPacketCRC = target.CalculateCRC();
            var match1 = new CRCResponse
            {
                Offset = 8,
                CRC32 = Crc32.CRC32Bytes(initPacket)
            };
            Assert.Equal(mock.ToBytes(match1), mock.ToBytes(initPacketCRC));

            mock.IsInitPacketSent = true;
            //target.Execute();
            var firmwareCRC = target.CalculateCRC();
            var match2 = new CRCResponse
            {
                Offset = 13,
                CRC32 = Crc32.CRC32Bytes(firmwarePart)
            };
            Assert.Equal(mock.ToBytes(match2), mock.ToBytes(firmwareCRC));
        }

        [Fact]
        public void OnReceivedDataTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            byte[] initPacket = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            byte[] firmwarePart = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

            mock.IsInitPacketSent = false;
            target.CreateCommandObject(30u);
            target.OnReceivedData(initPacket);
            Assert.True(CompareArray(initPacket, mock.InitPacket));

            mock.IsInitPacketSent = true;
            target.CreateDataObject(100u);
            target.OnReceivedData(firmwarePart);
            Assert.True(CompareArray(firmwarePart, mock.CurrentFirmwarePart));
        }

        [Fact]
        public void ExecuteTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            byte[] firmwarePart = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

            mock.IsInitPacketSent = true;
            target.CreateDataObject(100u);
            target.OnReceivedData(firmwarePart);
            Assert.True(CompareArray(firmwarePart, mock.CurrentFirmwarePart));
            Assert.False(mock.FirmwareParts.Any());

            target.Execute();
            Assert.True(mock.FirmwareParts.Any());
        }

        [Fact]
        public void SetPRNValueTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            target.SetPRNValue(12);

            Assert.Equal(12, mock.PRNValue);
        }

        [Fact]
        public void ToBytesTest()
        {
            var mock = new IDFUTargetMock(null);
            var target = mock.Mock();

            var selectResponse = new SelectResponse
            {
                MaxSize = 0,
                Offset = 10,
                CRC32 = 14356463
            };
            var crcResponse = new CRCResponse
            {
                Offset = 16,
                CRC32 = 73254134
            };

            var selectData = mock.ToBytes(selectResponse);
            var crcData = mock.ToBytes(crcResponse);

            var match1 = new List<byte>
            {
                0x60,06,01,
                0,0,0,0,
                10,0,0,0,
            };
            match1.AddRange(BitConverter.GetBytes(14356463));

            var match2 = new List<byte>
           {
                0x60,03,01,
                16,0,0,0,
           };
            match2.AddRange(BitConverter.GetBytes(73254134));

            Assert.True(CompareArray(match1, selectData));
            Assert.True(CompareArray(match2, crcData));
        }

        public bool CompareArray<T>(IEnumerable<T> arr1, IEnumerable<T> arr2)
        {
            if (arr1 == null || arr2 == null)
                return false;

            if (arr1.Count() != arr2.Count())
                return false;

            for (var i = 0; i < arr1.Count(); ++i)
            {
                if (!arr1.ElementAt(i).Equals(arr2.ElementAt(i)))
                {
                    return false;
                }
            }

            return true;
        }

    }
}
