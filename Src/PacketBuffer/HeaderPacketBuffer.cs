﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HTToolkit.PacketBuffer
{
    enum ParsingState
    {
        ParsingHeader,
        ParsingBody
    }

    public class HeaderPacketBuffer : IPacketBuffer
    {
        public int LengthBytes;
        public int PrefixBytes;
        public int SuffixBytes;
        public byte HeadDelimiter;
        public byte TailDelimiter;

        ParsingState state = ParsingState.ParsingHeader;
        int packetLength;
        List<byte> buffer = new List<byte>();

        public HeaderPacketBuffer() { }

        public event EventHandler<byte[]> PacketReceived;

        public void Accept(byte[] data)
        {
            buffer.AddRange(data);
            Parse();
        }

        void Parse()
        {
            while (true)
            {
                switch (state)
                {
                    case ParsingState.ParsingHeader:
                        int index = -1;
                        for (var i = 0; i < buffer.Count; ++i)
                        {
                            if (buffer[i] == HeadDelimiter)
                            {
                                index = i;
                                break;
                            }
                        }
                        if (index == -1)
                            buffer.Clear();
                        else if (index > 0)
                            buffer.RemoveRange(0, index);

                        if (buffer.Count >= PrefixBytes + LengthBytes)
                        {
                            packetLength = PrefixBytes + LengthBytes + buffer[PrefixBytes] + SuffixBytes;
                            state = ParsingState.ParsingBody;
                        }
                        else
                            return;
                        break;

                    case ParsingState.ParsingBody:
                        if (buffer.Count >= packetLength)
                        {
                            var packet = buffer.GetRange(0, packetLength);
                            buffer.RemoveRange(0, packetLength);
                            state = ParsingState.ParsingHeader;
                            if (packet[packetLength - 1] == TailDelimiter)
                                PacketReceived?.Invoke(this, packet.ToArray());
                            if (buffer.Count <= PrefixBytes + LengthBytes)
                                return;
                        }
                        else
                            return;
                        break;
                }
            }
        }
    }
}
