﻿using System;
using HTToolkit.BLE.NordicDFU;
using System.Linq;
using System.Threading.Tasks;
using HTToolkit.BLE.NordicDFU.Characteristics;

namespace HTToolkit.BLE.NordicDFU.Procedure
{
    public abstract class DFUProcedure
    {
        protected ControlPointCharacteristic controlPoint;
        protected PacketCharacteristic packetCharac;

        protected TaskCompletionSource<bool> completeTask = new TaskCompletionSource<bool>();

        protected DFUProcedure(ControlPointCharacteristic controlPoint,
                               PacketCharacteristic packetCharac)
        {
            this.controlPoint = controlPoint;
            this.packetCharac = packetCharac;
        }

        public virtual Task<bool> Start() { return completeTask.Task; }
    }
}
