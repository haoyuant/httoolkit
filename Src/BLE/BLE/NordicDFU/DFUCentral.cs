﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using HTToolkit.BLE.Central;
using HTToolkit.BLE.NordicDFU.Characteristics;
using HTToolkit.BLE.NordicDFU.Firmware;
using HTToolkit.BLE.NordicDFU.Procedure;

namespace HTToolkit.BLE.NordicDFU
{
    public class DFUCentral : CentralManagerBase
    {
        public const string SECURE_DFU_SERVICE = "FE59";
        public const string SECURE_DFU_CONTROL_POINT = "8EC90001-F315-4F60-9FB8-838830DAEA50";
        public const string SECURE_DFU_PACKET = "8EC90002-F315-4F60-9FB8-838830DAEA50";

        public ControlPointCharacteristic controlPoint;
        public PacketCharacteristic packetCharac;

        readonly IDFUDelegate Delegate;

        public DFUCentral(ICentralManager centralManager, IDFUDelegate _delegate = null) : base(centralManager)
        {
            Delegate = _delegate;

            controlPoint = new ControlPointCharacteristic(centralManager, SECURE_DFU_CONTROL_POINT);
            packetCharac = new PacketCharacteristic(centralManager, SECURE_DFU_PACKET);
            characteristics.Add(SECURE_DFU_CONTROL_POINT, controlPoint);
            characteristics.Add(SECURE_DFU_PACKET, packetCharac);
        }

        public async Task UpdateFirmware(IDFUFirmware firmware)
        {
            var initPacketProc = new TransferInitPacketProcedure(controlPoint, packetCharac, firmware.InitPacket);
            var firmwareProc = new TransferFirmwareProcedure(controlPoint, packetCharac, firmware.Firmware);
            firmwareProc.ProgressUpdated += FirmwareProc_ProgressUpdated;
            if(await initPacketProc.Start())
            {
                Delegate?.OnInitPacketSent();
                if (await firmwareProc.Start())
                {
                    Delegate?.OnFirmwareUpdated();
                    Debug.WriteLine("Device will be reset and ready to use soon.");
                }
                else
                    throw new ApplicationException("Failed to transfer firmware, please retry.");
            }
            else
                throw new ApplicationException("Failed to transfer init packet, please retry.");

        }

        void FirmwareProc_ProgressUpdated(object sender, double progress)
        {
            Debug.WriteLine("Update firmware progress: {0}%", progress * 100d);
            Delegate?.OnProgressUpdated(progress);
        }

        public void ScanDFUTargets()
        {
            centralManager.TryScan(SECURE_DFU_SERVICE);
        }

        public override void OnDiscoveredDevice(PeripheralProfile deviceInfo)
        {
            Delegate?.OnNewDFUTargetFound(deviceInfo);
        }

        public override Task<PeripheralProfile> Connect(string identifier, int timeout = 0)
        {
            StopScan();
            return base.Connect(identifier, timeout);
        }
    }
}
