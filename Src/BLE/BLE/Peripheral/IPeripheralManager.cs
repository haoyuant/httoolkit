﻿
namespace HTToolkit.BLE.Peripheral
{
    public interface IPeripheralManager
    {
        /// <summary>
        /// Init the peripheral manager's service tree and start advertising.
        /// If the current bluetooth state is on, this method will be executed immediately,
        /// otherwise, the method will be invoked when the bluetooth state changes to on.
        /// </summary>
        /// <param name="tree">
        /// A json representation of the service tree, this object should be obtained
        /// by using HTToolkit.BLE.ServiceTree to read a json file.
        /// </param>
        void StartAdvertising(ServiceTree tree);
        /// <summary>
        /// Stops the advertising.
        /// </summary>
        void StopAdvertising();
        /// <summary>
        /// Updates the value of a specific characteristic.
        /// </summary>
        /// <returns><c>true</c>, if value was updated, <c>false</c> otherwise.</returns>
        /// <param name="data">Binary format of the new value supposed to be sent.</param>
        /// <param name="uuid">UUID of the characteristic being updated.</param>
        bool UpdateValue(byte[] data, string uuid);
        /// <summary>
        /// Gets the current value of a specific characteristic.
        /// </summary>
        /// <returns>The value.</returns>
        /// <param name="uuid">UUID of the characteristic.</param>
        byte[] GetValue(string uuid);
        /// <summary>
        /// Release resources the peripheral manager is holding.
        /// After callin this method, this object should not be used again.
        /// A new instance should be created manually and used by the developer instead.
        /// </summary>
        void Release();
        /// <summary>
        /// Sets the dispatcher to relay BLE events.
        /// This api may change in the future, since it can be replaced by an interface instead of
        /// using a concrete class to relay the event.
        /// </summary>
        /// <param name="dispatcher">Dispatcher.</param>
        void SetDispatcher(PeripheralEventDispatcher dispatcher);
    }
}
