﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HTToolkit.BLE.Central
{
    public interface ICentralManager
    {
        /// <summary>
        /// A delegate used to handle BLE events
        /// </summary>
        /// <value>The BLEE vent delegate.</value>
        IBLEEventDelegate BLEEventDelegate { get; set; }
        /// <summary>
        /// Try scanning peripherals with specific service uuid, if the bluetooth state is on, it will start scanning immediately,
        /// otherwise, it will wait until the state becomes on.
        /// </summary>
        /// <returns><c>true</c>, if scan was tryed, <c>false</c> otherwise.</returns>
        /// <param name="serviceUUID">Service UUID.</param>
        bool TryScan(string serviceUUID);
        /// <summary>
        /// Stops the scan.
        /// </summary>
        void StopScan();
        /// <summary>
        /// Retrieves stored and connected devices from the os.
        /// </summary>
        /// <param name="uuids">Uuids.</param>
        void RetrieveDevices(List<string> uuids);
        /// <summary>
        /// Connect to a specified peripheral by specifying its uuid and set the connection timeout.
        /// </summary>
        /// <returns>The connected peripheral profile, null will be returned if fails to connect.</returns>
        /// <param name="identifier">UUID of the connecting peripheral.</param>
        /// <param name="timeout">Connection Timeout.</param>
        Task<PeripheralProfile> Connect(string identifier, int timeout = 0);
        /// <summary>
        /// Disconnect with the current connected peripheral and set the operation timeout.
        /// </summary>
        /// <returns>Awaitable task.</returns>
        /// <param name="timeout">Disconnection Timeout.</param>
        Task Disconnect(int timeout = 0);
         /// <summary>
         /// Read the value of a specific characteristic and set the read time out.
         /// </summary>
         /// <returns>Bytes of the characteristic value, null if fails to read.</returns>
         /// <param name="characteristicUUID">Characteristic UUID.</param>
         /// <param name="timeout">Read Timeout.</param>
        Task<byte[]> Read(string characteristicUUID, int timeout = 0);
        /// <summary>
        /// Enable or disable notification on a specific characteristic
        /// </summary>
        /// <param name="characteristicUUID">Characteristic UUID.</param>
        /// <param name="enabled">If set to <c>true</c> enabled.</param>
        void Subscribe(string characteristicUUID, bool enabled);
        /// <summary>
        /// Write data in bytes to a specific characterisic.
        /// </summary>
        /// <param name="characteristicUUID">Characteristic UUID.</param>
        /// <param name="data">Data to send.</param>
        /// <param name="withResponse">Use response or not.</param>
        void Write(string characteristicUUID, byte[] data, bool withResponse);
        /// <summary>
        /// Write data in json to a specific characterisic.
        /// </summary>
        /// <param name="characteristicUUID">Characteristic UUID.</param>
        /// <param name="json">Json to send.</param>
        /// <param name="withResponse">Use response or not.</param>
        void Write(string characteristicUUID, string json, bool withResponse);
        /// <summary>
        /// Reset the state of this CentralManager.
        /// </summary>
        void Clean();
    }
}
