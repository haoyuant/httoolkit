﻿using System;
using Newtonsoft.Json;

namespace HTToolkit.BLE
{
    [JsonObject]
    public class ServiceTree
    {
        [JsonProperty("advertising")]
        public string[] Advertising { get; set; }
        [JsonProperty("services")]
        public Service[] Services { get; set; }

        public static ServiceTree FromJson(string json)
        {
            return JsonConvert.DeserializeObject<ServiceTree>(json);
        }
    }

    [JsonObject]
    public class Service
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("characteristics")]
        public Characteristic[] Characteristics { get; set; }

    }

    [JsonObject]
    public class Characteristic
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("properties")]
        public string[] Properties { get; set; }
        [JsonProperty("permissions")]
        public string[] Permissions { get; set; }
    }
}
