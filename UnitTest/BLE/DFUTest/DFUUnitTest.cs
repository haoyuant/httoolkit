﻿using System;
using Moq;
using Xunit;
using HTToolkit.BLE;
using HTToolkit.BLE.NordicDFU;
using HTToolkit.BLE.Central;
using System.Collections.Generic;
using System.Linq;
using HTToolkit.BLE.NordicDFU.Characteristics;
using System.Threading.Tasks;

namespace DFUTest
{
    public class DFUUnitTest
    {
        ICentralManager centralManager;
        IDFUTarget dfuTarget;
        DFUCentral dfuCentral;
        ControlPointCharacteristic controlPoint;
        PacketCharacteristic packetCharac;

        byte[] writeContents;

        public DFUUnitTest()
        {
            var cmMock = new Mock<ICentralManager>();
            cmMock.Setup(m => m.Subscribe(It.IsAny<string>(), It.IsAny<bool>()))
                  .Callback((string cid, bool enable) =>
                   {
                       Console.WriteLine("Enable notification on characteristic {0}", cid);
                   });
            cmMock.Setup(m => m.Write(It.IsAny<string>(), 
                                      It.IsAny<byte[]>(),
                                      It.IsAny<bool>()))
                  .Callback<string, byte[], bool>((cid, data, response) =>
                   {
                       OnReceivedViaBluetooth(data, cid, response);
                   });
            centralManager = cmMock.Object;

            dfuCentral = new DFUCentral(centralManager);
            controlPoint = dfuCentral.controlPoint;
            packetCharac = dfuCentral.packetCharac;

            var dfuTargetMock = new IDFUTargetMock(dfuCentral);
            dfuTarget = dfuTargetMock.Mock();
        }

        [Fact]
        public void InitDFUCentralTest()
        {
            Assert.NotNull(dfuCentral.controlPoint);
            Assert.NotNull(dfuCentral.packetCharac);
        }

        #region ControlPointCharacteristic Test
        [Fact]
        public async Task SelectCommandObjectTest()
        {
            var dfuTargetMock = new IDFUTargetMock(dfuCentral);
            dfuTarget = dfuTargetMock.Mock();

            byte[] data = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            dfuTargetMock.InitPacket = new List<byte>();
            dfuTargetMock.InitPacket.AddRange(data);

            var response = await controlPoint.SelectCommandObject();
            Assert.True(CompareArray(writeContents, new byte[] { 06, 01 }));

            Assert.Equal(0u, response.MaxSize);
            Assert.Equal(9u, response.Offset);
            Assert.Equal(Crc32.CRC32Bytes(data), response.CRC32);
        }

        [Fact]
        public async Task SelectDataObjectTest()
        {
            var dfuTargetMock = new IDFUTargetMock(dfuCentral);
            dfuTarget = dfuTargetMock.Mock();

            byte[] data = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            dfuTargetMock.CurrentFirmwarePart = new List<byte>();
            dfuTargetMock.CurrentFirmwarePart.AddRange(data);

            //dfuTargetMock.IsInitPacketSent = true;
            //dfuTarget.Execute();

            var response = await controlPoint.SelectDataObject();
            Assert.True(CompareArray(writeContents, new byte[] { 06, 02 }));

            Assert.Equal(30u, response.MaxSize);
            Assert.Equal(12u, response.Offset);
            Assert.Equal(Crc32.CRC32Bytes(data), response.CRC32);
        }

        [Fact]
        public async Task CreateCommandObjectTest()
        {
            var dfuTargetMock = new IDFUTargetMock(dfuCentral);
            dfuTarget = dfuTargetMock.Mock();

            await controlPoint.CreateCommandObject(100u);
            Assert.True(CompareArray(writeContents, new byte[] { 01, 01, 100, 0, 0, 0 }));

            Assert.NotNull(dfuTargetMock.InitPacket);
        }

        [Fact]
        public async Task CreateDataObjectTest()
        {
            var dfuTargetMock = new IDFUTargetMock(dfuCentral);
            dfuTarget = dfuTargetMock.Mock();

            await controlPoint.CreateDataObject(100u);
            Assert.True(CompareArray(writeContents, new byte[] { 01, 02, 100, 0, 0, 0 }));

            Assert.NotNull(dfuTargetMock.CurrentFirmwarePart);
        }

        [Fact]
        public async Task CalculateCRCTest()
        {
            var mock = new IDFUTargetMock(dfuCentral);
            dfuTarget = mock.Mock();

            await controlPoint.CalculateCRC();
            Assert.True(CompareArray(writeContents, new byte[] { 03 }));


            byte[] initPacket = { 0, 1, 2, 3, 4, 5, 6, 7 };
            byte[] firmwarePart = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            mock.InitPacket = new List<byte>();
            mock.InitPacket.AddRange(initPacket);
            mock.CurrentFirmwarePart = new List<byte>();
            mock.CurrentFirmwarePart.AddRange(firmwarePart);

            mock.IsInitPacketSent = false;
            var initPacketCRC = dfuTarget.CalculateCRC();
            var match1 = new CRCResponse
            {
                Offset = 8,
                CRC32 = Crc32.CRC32Bytes(initPacket)
            };
            Assert.Equal(mock.ToBytes(match1), mock.ToBytes(initPacketCRC));

            mock.IsInitPacketSent = true;
            //dfuTarget.Execute();
            var firmwareCRC = dfuTarget.CalculateCRC();
            var match2 = new CRCResponse
            {
                Offset = 13,
                CRC32 = Crc32.CRC32Bytes(firmwarePart)
            };
            Assert.Equal(mock.ToBytes(match2), mock.ToBytes(firmwareCRC));
        }


        [Fact]
        public async Task ExecuteTest()
        {
            var mock = new IDFUTargetMock(dfuCentral);
            dfuTarget = mock.Mock();

            byte[] firmwarePart = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

            mock.IsInitPacketSent = true;
            dfuTarget.CreateDataObject(100u);
            dfuTarget.OnReceivedData(firmwarePart);
            Assert.True(CompareArray(firmwarePart, mock.CurrentFirmwarePart));
            Assert.False(mock.FirmwareParts.Any());

            var result = await controlPoint.ExecuteCommand();

            Assert.True(CompareArray(writeContents, new byte[] { 04 }));

            Assert.True(mock.FirmwareParts.Any());
            Assert.Equal(14, mock.FirmwareParts[0].Count);
        }

        [Fact]
        public async Task SetPRNValueTest()
        {
            var mock = new IDFUTargetMock(dfuCentral);
            dfuTarget = mock.Mock();

            await controlPoint.SetPRNValue(12);
            Assert.True(CompareArray(writeContents, new byte[] { 02, 12, 0 }));

            Assert.Equal(12, mock.PRNValue);
        }
        #endregion

        #region PacketCharacteristic Test
        [Fact]
        public void SendInitPacketTest()
        {
            var mock = new IDFUTargetMock(dfuCentral);
            dfuTarget = mock.Mock();

            dfuTarget.CreateCommandObject(30);

            byte[] initPacket = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            byte[] dataToSend = { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            packetCharac.SendInitPacket(initPacket, 5);

            var packetsSent = dataToSend.Length / packetCharac.PacketSize;
            var lastDataSent = dataToSend.Skip((int)(packetsSent * packetCharac.PacketSize));
            if (lastDataSent.Count() != 0)
                Assert.True(CompareArray(lastDataSent, writeContents));

            Assert.True(CompareArray(dataToSend, mock.InitPacket));
        }
        #endregion
        #region DFU Procedure Test
        //[Fact]
        //public void UpdateFirmwareTest()
        //{
        //    var mock = new IDFUTargetMock(dfuCentral);
        //    dfuTarget = mock.Mock();

        //    StartUpdateFirmwareProc(mock);
        //}

        //[Fact]
        //public void UpdateFirmwareStartWithCompletePacketTest()
        //{
        //    var mock = new IDFUTargetMock(dfuCentral);
        //    dfuTarget = mock.Mock();

        //    var part1 = new byte[30];
        //    var part2 = new byte[30];
        //    for (var i = 0; i < 30; ++i)
        //    {
        //        part1[i] = (byte)(i % 200);
        //        part2[i] = (byte)((i + 30) % 200);
        //    }
        //    mock.FirmwareParts.Add(part1.ToList());
        //    mock.FirmwareParts.Add(part2.ToList());

        //    StartUpdateFirmwareProc(mock);
        //}

        //[Fact]
        //public void UpdateFirmwareStartWithIncompletePacketTest()
        //{
        //    var mock = new IDFUTargetMock(dfuCentral);
        //    dfuTarget = mock.Mock();

        //    var part1 = new byte[30];
        //    var part2 = new byte[30];
        //    for (var i = 0; i < 30; ++i)
        //    {
        //        part1[i] = (byte)(i % 200);
        //        part2[i] = (byte)((i + 30) % 200);
        //    }
        //    //mock.FirmwareParts.Add(part1.ToList());
        //    mock.CurrentFirmwarePart = new List<byte>();
        //    mock.CurrentFirmwarePart.AddRange(part1.Take(5));
        //    mock.CurrentFirmwarePart.AddRange(part2.Take(25));

        //    StartUpdateFirmwareProc(mock);
        //}

        //private async Task StartUpdateFirmwareProc(IDFUTargetMock mock)
        //{
        //    byte[] initPacket = new byte[1000];
        //    for (var i = 0; i < 1000; ++i)
        //    {
        //        initPacket[i] = (byte)(i % 200);
        //    }
        //    byte[] firmware = new byte[10000];
        //    for (var i = 0; i < 10000; ++i)
        //    {
        //        firmware[i] = (byte)(i % 200);
        //    }
        //    Assert.False(CompareArray(initPacket, mock.InitPacket));
        //    Assert.False(mock.IsInitPacketSent);
        //    string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
        //                               @"Firmware/flite_receiver_app_V15_dfu_package_v 1.21-0-g563558f.zip");
        //    var application = DFUApplication.FromZip(path);
        //    await dfuCentral.UpdateFirmware(application);

        //    Assert.True(CompareArray(initPacket, mock.InitPacket));
        //    Assert.True(mock.IsInitPacketSent);

        //    Assert.Equal(334, mock.FirmwareParts.Count);
        //}
        #endregion

        public void OnReceivedViaBluetooth(byte[] data, string cid, bool response)
        {
            writeContents = data;
            //return;
            dfuCentral.OnWrote(cid, true);
            switch (cid)
            {
                case DFUCentral.SECURE_DFU_CONTROL_POINT:
                    OnWroteControlPointCharac(data);
                    break;
                case DFUCentral.SECURE_DFU_PACKET:
                    OnWrotePacketCharac(data);
                    break;
            }
        }


        public void OnWroteControlPointCharac(byte[] data)
        {
            //SELECT_COMMAND_OBJECT
            if (CompareArray(data, new byte[] { 06, 01 }))
            {
                dfuTarget.SelectCommandObject();
            }
            //SELECT_DATA_OBJECT
            if (CompareArray(data, new byte[] { 06, 02 }))
            {
                dfuTarget.SelectDataObject();
            }
            //CREATE_COMMAND_OBJECT
            else if (CompareArray(data.Take(2), new byte[] { 01, 01 }))
            {
                dfuTarget.CreateCommandObject(BitConverter.ToUInt32(data, 2));
            }
            //CREATE_DATA_OBJECT
            else if (CompareArray(data.Take(2), new byte[] { 01, 02 }))
            {
                dfuTarget.CreateDataObject(BitConverter.ToUInt32(data, 2));
            }
            //CALCULATE_CRC
            else if (CompareArray(data, new byte[] { 03 }))
            {
                dfuTarget.CalculateCRC();
            }
            //EXECUTE
            else if (CompareArray(data, new byte[] { 04 }))
            {
                dfuTarget.Execute();
            }
            //SET_PRN_VALUE
            else if (CompareArray(data.Take(1), new byte[] { 02 }))
            {
                dfuTarget.SetPRNValue(BitConverter.ToUInt16(data, 1));
            }
        }

        public void OnWrotePacketCharac(byte[] data)
        {
            dfuTarget.OnReceivedData(data);
        }

        public bool CompareArray<T>(IEnumerable<T> arr1, IEnumerable<T> arr2)
        {
            if (arr1 == null || arr2 == null)
                return false;

            if (arr1.Count() != arr2.Count())
                return false;

            for (var i = 0; i < arr1.Count(); ++i)
            {
                if (!arr1.ElementAt(i).Equals(arr2.ElementAt(i)))
                {
                    return false;
                }
            }

            return true;
        }

        [Fact]
        public void SendPacketTest()
        {
            string uuidString = "abc";
            var _centralManagerMock = new Mock<ICentralManager>();
            PacketCharacteristic DUT = new PacketCharacteristic(_centralManagerMock.Object, uuidString);
            List<byte> actualBytes = new List<byte>();
            byte[] expectedData = new byte[] { 1, 2, 3, 4, 5 };

            _centralManagerMock.Setup(a => a.Write(uuidString, It.IsAny<byte[]>(), It.IsAny<bool>())).
            Callback((string _str, byte[] _bytes, bool _bool) =>
            {
                actualBytes.AddRange(_bytes);
            }
            );

            DUT.SendPacket(expectedData);

            Assert.Equal(true, actualBytes.SequenceEqual(expectedData));

        }
    }
}
