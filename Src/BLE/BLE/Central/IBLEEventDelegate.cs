﻿using System;
namespace HTToolkit.BLE.Central
{
    public interface IBLEEventDelegate
    {
        void OnDiscoveredDevice(PeripheralProfile deviceInfo);
        void OnDiscoveredCharacteristic();
        void OnWrote(string cid, bool success);
        void OnValueUpdated(CharacteristicUpdate update);
    }
}
