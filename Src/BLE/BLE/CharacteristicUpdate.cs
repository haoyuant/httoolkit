﻿using System;
namespace HTToolkit.BLE
{
    public struct CharacteristicUpdate
    {
        public string UUID;
        public byte[] Data;
    }
}
