﻿using System;
using System.Collections.Generic;

namespace HTToolkit.BLE.NordicDFU.Characteristics
{
    public static class ControlPointCommandFactory
    {
        public static byte[] GetBytesOfSelectCommandObject()
        {
            return GetBytesOfSelectObject(ObjectType.Command);
        }

        public static byte[] GetBytesOfSelectDataObject()
        {
            return GetBytesOfSelectObject(ObjectType.Data);
        }

        static byte[] GetBytesOfSelectObject(ObjectType type)
        {
            return new byte[] { (byte)CommandCode.Select, (byte)type };
        }

        public static byte[] GetBytesOfCreateCommandOject(uint size)
        {
            return GetBytesOfCreateObject(ObjectType.Command, size);
        }

        public static byte[] GetBytesOfCreateDataOject(uint size)
        {
            return GetBytesOfCreateObject(ObjectType.Data, size);
        }

        static byte[] GetBytesOfCreateObject(ObjectType type, uint size)
        {
            var createCommand = new List<byte>
            {
                (byte)CommandCode.Create,
                (byte)type
            };
            createCommand.AddRange(BitConverter.GetBytes(size));
            return createCommand.ToArray();
        }

        public static byte[] GetBytesOfCalculateCRC()
        {
            return new byte[] { (byte)CommandCode.CRC };
        }

        public static byte[] GetBytesOfExecute()
        {
            return new byte[] { (byte)CommandCode.Execute };
        }

        public static byte[] GetBytesOfSetPRNValue(ushort value)
        {
            var command = new List<byte> { (byte)CommandCode.SetReceiptNotification };
            var bytes = BitConverter.GetBytes(value);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }
            command.AddRange(bytes);

            return command.ToArray();
        }
    }
}
