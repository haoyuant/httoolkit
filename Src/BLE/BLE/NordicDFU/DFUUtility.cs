﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace  HTToolkit.BLE.NordicDFU
{
    public static class DFUUtility
    {
        public static List<byte[]> SplitFirmwareParts(byte[] data, uint packetLength)
        {
            var totalBytes = (uint)data.Length;
            uint partIdx = 0;
            var parts = new List<byte[]>();
            while (totalBytes > 0)
            {
                IEnumerable<byte> packet = null;
                if (totalBytes > packetLength)
                {
                    totalBytes -= packetLength;
                    packet = data.Skip((int)(partIdx * packetLength)).Take((int)packetLength);
                }
                else
                {
                    packet = data.Skip((int)(partIdx * packetLength)).Take((int)totalBytes);
                    totalBytes = 0;
                }
                parts.Add(packet.ToArray());
                ++partIdx;
            }
            return parts;
        }

        public static uint CalculatePartIdx(uint offset, uint packetLength)
        {
            return offset > 0 ? (offset - 1) / packetLength : 0;
        }

        public static byte[] GetDataNeedToBeSentOfPart(List<byte[]> parts, uint offset, uint packetLength)
        {
            var partIdx = CalculatePartIdx(offset, packetLength);
            var part = parts[(int)partIdx];
            var upperBound = part.Length + partIdx * packetLength;
            var remainCount = upperBound - offset;
            var offsetIdxInPart = packetLength - remainCount;
            return part.Skip((int)offsetIdxInPart).ToArray();
        }

        public static byte[] GetDataNeedToBeSentOfPart(List<byte[]> parts, uint partIdx)
        {
            return parts[(int)partIdx].ToArray();
        }


        public static bool VerifyCRC(byte[] data, uint offset, uint crc)
        {
            var dataToVerify = data.Take((int)offset).Reverse().ToArray();
            return Crc32.CRC32Bytes(dataToVerify) == crc;
            //return true;
        }
    }
}
