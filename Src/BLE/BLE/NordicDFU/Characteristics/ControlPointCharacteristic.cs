﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using HTToolkit.BLE;
using HTToolkit.BLE.Central;
using System.Diagnostics;

namespace HTToolkit.BLE.NordicDFU.Characteristics
{
    public class ControlPointCharacteristic : HTCharacteristic
    {
        TaskCompletionSource<SelectResponse> selectTask;
        TaskCompletionSource<bool> createTask;
        TaskCompletionSource<bool> executeTask;
        TaskCompletionSource<CRCResponse> crcTask;

        public ushort PRNValue { get; set; }

        public ControlPointCharacteristic(ICentralManager centralManager, string uuid) : base(centralManager, uuid)
        {
        }

        public override void OnReadyToUse()
        {
            Subscribe(true);
        }

        public async Task<SelectResponse> SelectCommandObject()
        {
            var selectCommand = ControlPointCommandFactory.GetBytesOfSelectCommandObject();
            return await SelectObject(selectCommand);
        }

        public async Task<SelectResponse> SelectDataObject()
        {
            var selectCommand = ControlPointCommandFactory.GetBytesOfSelectDataObject();
            return await SelectObject(selectCommand);
        }

        async Task<SelectResponse> SelectObject(byte[] selectCommand)
        {
            selectTask = new TaskCompletionSource<SelectResponse>();
            await WriteWithResponseAsync(selectCommand);

            return await selectTask.Task;
        }

        public async Task<bool> CreateCommandObject(UInt32 size)
        {
            var createCommand = ControlPointCommandFactory.GetBytesOfCreateCommandOject(size);
            return await CreateObject(createCommand);
        }

        public async Task<bool> CreateDataObject(UInt32 size)
        {
            var createCommand = ControlPointCommandFactory.GetBytesOfCreateDataOject(size);
            return await CreateObject(createCommand);
        }

        async Task<bool> CreateObject(byte[] createCommand)
        {
            createTask = new TaskCompletionSource<bool>();

            await WriteWithResponseAsync(createCommand);

            return await createTask.Task;
        }

        public async Task<CRCResponse> CalculateCRC()
        {
            crcTask = new TaskCompletionSource<CRCResponse>();

            var crcCommand = ControlPointCommandFactory.GetBytesOfCalculateCRC();
            await WriteWithResponseAsync(crcCommand);

            return await crcTask.Task;
        }

        public async Task<bool> ExecuteCommand()
        {
            executeTask = new TaskCompletionSource<bool>();

            var executeCommand = ControlPointCommandFactory.GetBytesOfExecute();
            await WriteWithResponseAsync(executeCommand);

            return await executeTask.Task;
        }

        public async Task SetPRNValue(ushort value)
        {
            var command = ControlPointCommandFactory.GetBytesOfSetPRNValue(value);
            await WriteWithResponseAsync(command.ToArray());

            PRNValue = value;
        }

        void OnReceivedCalculateCRCResponse(byte[] data)
        {
            var crcResponse = new CRCResponse
            {
                Offset = BitConverter.ToUInt32(data, 3),
                CRC32 = BitConverter.ToUInt32(data, 7)
            };
            crcTask?.TrySetResult(crcResponse);
        }

        void OnReceivedExecuteResponse(CommandResultCode result)
        {
            executeTask?.TrySetResult(result == CommandResultCode.Success);
        }

        void OnReceivedSelectCommandObjectResponse(byte[] data)
        {
            var selectResponse = new SelectResponse
            {
                MaxSize = BitConverter.ToUInt32(data, 3),
                Offset = BitConverter.ToUInt32(data, 7),
                CRC32 = BitConverter.ToUInt32(data, 11)
            };
            selectTask?.TrySetResult(selectResponse);
        }

        void OnReceivedCreateCommandObjectResponse(CommandResultCode result)
        {
            createTask?.TrySetResult(result == CommandResultCode.Success);
        }

        public override void OnValueUpdated(byte[] data)
        {
            if (data[0] == (byte)CommandCode.Response)
            {
                var command = (CommandCode)data[1];
                var result = (CommandResultCode)data[2];
                switch (command)
                {
                    case CommandCode.Create:
                        OnReceivedCreateCommandObjectResponse(result);
                        break;
                    case CommandCode.Select:
                        OnReceivedSelectCommandObjectResponse(data);
                        break;
                    case CommandCode.Execute:
                        OnReceivedExecuteResponse(result);
                        break;
                    case CommandCode.CRC:
                        OnReceivedCalculateCRCResponse(data);
                        break;
                }
                //PrintReceiveData(data);
            }
        }

        void PrintReceiveData(byte[] data)
        {
            Debug.WriteLine("Received response: ");
            foreach (var b in data)
            {
                Debug.Write(b + " ");
            }
            Debug.WriteLine("");
        }
    }
}
