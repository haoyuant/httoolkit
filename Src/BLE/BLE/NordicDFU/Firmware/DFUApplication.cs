﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.IO.Compression;
using System.Linq;

namespace HTToolkit.BLE.NordicDFU.Firmware
{
    public class DFUApplication : IDFUFirmware
    {
        const string MANIFEST_FILE = "manifest.json";

        public DFUFirmwareInfo Info { get; private set; }
        public byte[] InitPacket { get; private set; }
        public byte[] Firmware { get; private set; }

        public DFUApplication() { }

        public DFUApplication(DFUFirmwareInfo info, byte[] initPacket, byte[] firmware)
        {
            Info = info;
            InitPacket = initPacket;
            Firmware = firmware;
        }

        public static DFUApplication FromZip(string filePath)
        {
            if (File.Exists(filePath))
            {
                var fileInfo = new FileInfo(filePath);
                var fileSize = fileInfo.Length;

                using (var zip = ZipFile.OpenRead(filePath))
                {
                    var unzipDest = Path.Combine(Path.GetTempPath(), "Firmwares");
                    if (Directory.Exists(unzipDest))
                    {
                        Directory.Delete(unzipDest, true);
                    }
                    zip.ExtractToDirectory(unzipDest);

                    var manifestEntry = Path.Combine(unzipDest, MANIFEST_FILE);
                    if (File.Exists(manifestEntry))
                    {
                        var contents = File.ReadAllText(manifestEntry);
                        var manifest = JsonConvert.DeserializeObject<DFUApplicationManifest>(contents);
                        var datEntry = Path.Combine(unzipDest, manifest.Manifest.Application.Dat);
                        var binEntry = Path.Combine(unzipDest, manifest.Manifest.Application.Bin);

                        var fileName = Path.GetFileName(filePath);
                        var type = DFUFirmwareType.Application;
                        var size = string.Format("{0:0.00} kB", (double)fileSize / 1024);
                        var version = manifest.Manifest.Version;

                        var application = new DFUApplication
                        {
                            Info = new DFUFirmwareInfo(fileName, type, size, version),
                            InitPacket = File.ReadAllBytes(datEntry),
                            Firmware = File.ReadAllBytes(binEntry),
                        };
                        return application;
                    }
                }
            }
            return null;
        }

        //static byte[] ReadBytesOfZipEntry(ZipEntry entry)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        entry.Extract(ms);
        //        ms.Position = 0;
        //        var size = ms.Length;
        //        var data = new byte[size];
        //        ms.Read(data, 0, (int)size);
        //        return data;
        //    }
        //}
    }
}
